$("#changeLanguage").on("click", function () {
    var id = $(this).data('id');
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: '/lang' ,
        type: 'post',
        data: {
            'lang': id
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(response){
        var current_link = window.location.href;
        window.location = current_link;
    }).fail(function(){
        console.log("Redirecting error please fix the issue");
    }).always(function(){
        var current_link = window.location.href.split('#')[0];
        window.location = current_link;
    });
});

function appAlert(message, status) {
    var container = ".c-content-panel";
    var place = "prepend";
    status = (status !== 'undefined') ? status : 'danger';
    // alert(message);
    App.alert({
        type: status,
        icon: status,
        message: message,
        container: container,
        place: place
    });
}
function validation_check(data) {
    if (data.validation) {
        if($("html:lang(bn)").length){
            appAlert("যাচাইকরণ ত্রুটি খুঁজে পাওয়া গিয়েছে", "danger");
        }else {
            appAlert("Validation error found", "danger");
        }
        $(".form-group").removeClass("has-error").children("div").children(".help-block").remove();
        var errors = data.errors;
        for (var field in errors) {
            var group = $(".has-error-" + field);
            if (group.length) {
                if (!group.hasClass("has-error")) {
                    group.addClass("has-error");
                }
                //group.children("div").append("<span class='help-block'><strong>" + errors[field][0] + "</strong></span>");
            }else{
              var data = field.split(".");
              data = data[0]+'-'+data[1];
              var group_new = $(".has-error-" + data);
              if (group_new.length) {
                if (!group_new.hasClass("has-error")) {
                    group_new.addClass("has-error");
                }
              }
              group_new.addClass("has-error");
            }
        }
    }
}

$("#ajaxForm, .ajaxForm").on("submit", function (e) {
    e.preventDefault();
    var form_data = new FormData(this);
    var form = $(this);
    var action = form.children('input[name="_action"]').val();
    var method = form.children('input[name="_method"]').val();

    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } });
    setTimeout($.unblockUI, 5000);

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: action,
        type: method,
        data: form_data,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (response) {
        $(document).ajaxStop($.unblockUI);
        if (response.success) {
            $(".has-error").children(".help-block").remove();
            appAlert(response.message, 'success');
            if (response.data !== undefined) {
                if (response.data.redirect_to !== undefined) {
                    window.location = response.data.redirect_to;
                }
            }
        } else if(response.validation) {
            validation_check(response);
        }else {
            appAlert(response.message, 'danger');
        }
    }).fail(function (response) {
        appAlert(response.message, "danger");
    });
});
$(".select_dropdown").on("change", function (e) {
    e.preventDefault();
    var current = $(this);
    var target = $(this).data('target');
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: current.data('action'),
        method: 'post',
        data: {
            'id': current.val()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function (response) {
        console.log(response.data);
        var option_text = $(target + " option:first-child").html();
        html = '<option value="0">' + option_text + '</option>';
        $.each(response.data, function (index, value) {
            html += '<option value="' + value.id + '">' + value.name + '</option>';
        });
        $(target).html(html);
    }).fail(function (response) {
        appAlert(response.message, "danger");
    });
});
$(document).ready(function(){
    NProgress.done();
});
