$('body').on("change", "select[name^=age]", function(){
  var age = $(this).val();
  var count = $(this).data('count');

  $('#child-'+count+' input[name^=art]').prop("disabled", true);
  $('#child-'+count+' input[name^=craft]').prop("disabled", true);
  $('#child-'+count+' input[name^=writing]').prop("disabled", true);

  $('#child-'+count+' input[name^=art]').each(function () {
		$(this).removeAttr('checked');
		$('input[type="checkbox"]').prop('checked', false);
	});
  $('#child-'+count+' input[name^=craft]').each(function () {
		$(this).removeAttr('checked');
		$('input[type="checkbox"]').prop('checked', false);
	});
  $('#child-'+count+' input[name^=writing]').each(function () {
		$(this).removeAttr('checked');
		$('input[type="checkbox"]').prop('checked', false);
	});

  if(age>=3 && age<=6){
    $('#child-'+count+' input[id^=art_17_1_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=art_18_1_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_17_1_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_18_1_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_17_1_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_18_1_'+count+']').prop("disabled", false);
  }
  else if(age>=7 && age<=10){
    $('#child-'+count+' input[id^=art_17_2_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=art_18_2_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_17_2_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_18_2_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_17_2_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_18_2_'+count+']').prop("disabled", false);
  }
  else if(age>=11 && age<=15){
    $('#child-'+count+' input[id^=art_17_3_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=art_18_3_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_17_3_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=craft_18_3_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_17_3_'+count+']').prop("disabled", false);
    $('#child-'+count+' input[id^=writing_18_3_'+count+']').prop("disabled", false);
  }
  else{
      alert("Please select a valid age!");
  }
});
$("#btn-add-more-child").click(function(){
    var current = $(this);
    /* check if the last child has name & age first */
    var listElement = $( "#appendChild" ).find(".child:last");

    console.log("name : "+listElement.find("input[name^=name]").val());
    console.log("age : "+listElement.find("input[name^=age]").val());

    if( listElement.find("input[name^=name]").val() == "" || listElement.find("input[name^=age]").val() == ""){
      alert("Fill up the previous section first!");
    }else{
      $.ajax({
          url : current.data("url")+"/"+global_checkbox_var,
          method : 'GET',
      }).done(function (response) {
          $("#appendChild").append(response);

          $('.date-picker').each(function(){
              $(this).datepicker({
                  rtl: $(this).data('rtl'),
                  orientation: "left",
                  autoclose: true,
                  container: $(this),
                  format: $(this).data('date-format'),
              });
          });

        $('#appendChild').find('#child-'+global_checkbox_var+' input[name^=art]').prop("disabled", true);
        $('#appendChild').find('#child-'+global_checkbox_var+' input[name^=craft]').prop("disabled", true);
        $('#appendChild').find('#child-'+global_checkbox_var+' input[name^=writing]').prop("disabled", true);

        global_checkbox_var++;
      }).fail(function (response) {
          appAlert(response.message, "danger");
      });
    }
});
