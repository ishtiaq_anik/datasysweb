<?php

use Illuminate\Database\Seeder;

class SlotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slots')->insert([
            'day' 		 => '2017-11-17',
            'day_bangla' => '১৭ নভেম্বর (শুক্রবার)',
            'date'		 => '2017-11-17',
            'status' 	 => 'active',
            'updated_at' => new DateTime,
            'created_at' => new DateTime,
        ]);
        DB::table('slots')->insert([
            'day' 		 => '2017-11-18',
            'day_bangla' => '১৮ নভেম্বর (শনিবার)',
            'date' 		 => '2017-11-18',
            'status' 	 => 'active',
            'updated_at' => new DateTime,
            'created_at' => new DateTime,
        ]);
    }
}
