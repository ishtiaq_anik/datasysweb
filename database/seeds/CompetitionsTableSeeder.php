<?php

use Illuminate\Database\Seeder;

class CompetitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competitions')->insert([
            'name' => 'Art Competition',
            'name_bangla' => 'চিত্র অংকন প্রতিযোগিতা',
            'status' => 'active',
            'updated_at' => new DateTime,
            'created_at' => new DateTime,
        ]);
        DB::table('competitions')->insert([
            'name' => 'Collage Craft Competition',
            'name_bangla' => 'কোলাজ ক্রাফট প্রতিযোগিতা',
            'status' => 'active',
            'updated_at' => new DateTime,
            'created_at' => new DateTime,
        ]);
        DB::table('competitions')->insert([
            'name' => 'Hand Writing Competition',
            'name_bangla' => 'সুন্দর হাতের লেখা প্রতিযোগিতা',
            'status' => 'active',
            'updated_at' => new DateTime,
            'created_at' => new DateTime,
        ]);
    }
}
