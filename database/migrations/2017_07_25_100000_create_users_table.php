<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('district_id')->index()->nullable();
            $table->integer('office_id')->index()->nullable();
            $table->integer('group_id')->index()->nullable();
            $table->string('slug')->nullable();
            $table->string('name');
            $table->string('name_bangla')->nullable();
            $table->string('picture')->nullable();
            $table->string('signature')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('status')->default("active");
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
