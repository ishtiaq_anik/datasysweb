<?php

Auth::routes();
Route::get('/registration', 'ParticipantController@applyForm');
Route::get('/register', 'ParticipantController@applyForm');
Route::get('/password/reset', 'ParticipantController@applyForm');

Route::get('/', 'ParticipantController@applyForm')->name('apply');

Route::post('/apply/store', 'ParticipantController@applyStore')->name('apply_store');

Route::get('/add/children/{global}', 'ParticipantController@addMoreTemplate')->name('add_more_child');

Route::get('/success', 'ParticipantController@displaySuccess')->name('success');

Route::group(['middleware' => 'auth'],function() {
  Route::get('/dashboard', 'ParticipantController@index')->name('dashboard');
  Route::get('/dashboard/participants', 'ParticipantController@index')->name('dashboard');
  Route::post('/participants/dataload', 'ParticipantController@datatable')->name('participants_load');
  Route::get('/dashboard/entries', 'EntryController@index')->name('entries_load');
  Route::post('/entries/dataload', 'EntryController@datatable')->name('entries_load');
});

Route::get('/home', 'ParticipantController@applyForm')->name('home');
