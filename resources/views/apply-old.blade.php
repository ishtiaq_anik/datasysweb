@extends('layouts.master')

@section('title', __('titles.apply.title'))



@section('page_style_plugin')
<!-- BEGIN: BASE PLUGINS  -->
<link href="/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
<!-- END: BASE PLUGINS -->
<!-- BEGIN: PAGE STYLES -->
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END: PAGE STYLES -->
@endsection

@section('page_style')
@endsection

@section('content')

<section class="c-layout-revo-slider c-layout-revo-slider-1" dir="ltr">
  <div class="tp-banner-container tp-fullscreen tp-fullscreen-mobile">
    <div class="tp-banner rev_slider" data-version="5.0">

      <ul>
        <!-- SLIDE #1 -->
        <li data-transition="fade">
          <!-- MAIN IMAGE -->
          <img src="assets/base/img/content/backgrounds/cover.png"  alt="" >
          <!-- LAYER NR. 1 -->
          <div class="tp-caption customin customout"
          data-x="center"
          data-y="center"
          data-hoffset=""
          data-voffset="-30"

          data-start="1500"

          data-transform_in="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
          data-transform_out="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
          data-splitin="none"
          data-splitout="none"
          data-elementdelay="0.1"
          data-endelementdelay="0.1"
          data-endspeed="600"
          >
          <!-- <h3 class="c-main-title c-font-55 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
          Meet JANGO<br>
          The Ever Growing HTML5 Theme
        </h3> -->
      </div>
      <!-- LAYER NR. 2 -->
      <div class="tp-caption randomrotateout"
      data-x="center"
      data-y="center"
      data-hoffset=""
      data-voffset="120"
      data-transform_in="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
      data-transform_out="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
      data-start="2000"
      data-easing="Back.easeOut">
      <!-- <a href="#" class="c-action-btn btn btn-lg c-btn-square c-btn-border-2x c-btn-white c-btn-bold c-btn-uppercase">Explore</a> -->
    </div>
  </li>
  <!-- END SLIDE #1 -->
  <!-- SLIDE #2 -->
  <li data-transition="fade">
    <!-- MAIN IMAGE -->
    <img src="assets/base/img/content/backgrounds/s19.png"  alt="" >
    <!-- LAYER NR. 1 -->
    <div class="tp-caption customin customout"
    data-x="center"
    data-y="center"
    data-hoffset=""
    data-voffset="-30"

    data-start="1500"
    data-transform_in="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
    data-transform_out="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Back.easeOut;"
    data-splitin="none"
    data-splitout="none"
    data-elementdelay="0.1"
    data-endelementdelay="0.1"
    data-endspeed="600"
    >
    <!-- <h3 class="c-main-title c-font-55 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
    One JANGO<br>
    For every need
  </h3> -->
</div>
<!-- LAYER NR. 2 -->
<div class="tp-caption randomrotateout"
data-x="center"
data-y="center"
data-hoffset=""
data-voffset="120"
data-transform_in="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Power2.easeInOut;"
data-transform_out="x:0;y:0;z:0;rX:0.5;rY:0;rZ:0;sX:0.75;sY:0.75;skX:0;skY:0;opacity:0;s:600;e:Back.easeOut;"
data-start="2000"
data-easing="Back.easeOut">
<!-- <a href="#" class="c-action-btn btn btn-lg c-btn-square c-btn-border-2x c-btn-white c-btn-bold c-btn-uppercase">Explore</a> -->
</div>
</li>
<!-- END SLIDE #2 -->
</ul>
</div><!-- END REVOLUTION SLIDER -->
</div><!-- END OF SLIDER WRAPPER -->
</section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-1 -->




<div class="container">
  <div class="content">
    <section class="form-header">
      <!-- BEGIN: CONTENT/BARS/BAR-3 -->
      <div class="c-content-box c-size-md c-bg-red-fresh">
        <div class="container-fluid main-box">
          <div class="c-content-bar-3">
            <div class="row">
              <div class="col-md-8">
                <div class="c-content-title-1">
                  <h3 class="c-font-uppercase c-font-bold">Farm Fresh Children Day 2017</h3>
                  <p class="c-font-uppercase c-font-grey">Registration is closed</p>
                </div>
              </div>
              <div class="col-md-3 col-md-offset-1">
                <div class="c-content-v-center" >
                  <div class="c-wrapper">
                    <div class="c-body">
                      <a href="#registration" class="btn btn-lg c-btn-white c-font-uppercase c-font-bold c-btn-square c-btn-border-1x btn-register">@lang('home.apply.apply_now')</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END: CONTENT/BARS/BAR-3 -->
    </section>
    <section id="registration" class="apply-form c-bg-grey-1">
      <!-- BEGIN: PAGE CONTENT -->
      <div class="c-content-box c-size-md c-bg-grey-1" style="">
        <div class="container-fluid main-box">
          <div class="row">
            <div class="col-lg-12">



              <div class="c-content-panel">

                <div class="c-body c-bg-grey">
                  <div class="c-content-tab-1 c-theme c-margin-t-30">
                    <div class="nav-justified">
                      <ul class="nav nav-tabs nav-justified c-font-uppercase c-font-bold">
                        <li class="active"><a href="#tab_3_1_content" data-toggle="tab" class="c-border-red" aria-expanded="false">নিবন্ধন করুন</a></li>
                        <li class=""><a href="#tab_3_2_content" data-toggle="tab" class="c-border-red" aria-expanded="false">নিবন্ধন নিয়মাবলি</a></li>
                      </ul>
                    </div>
                    <div class="tab-content c-bordered c-padding-lg">
                      <div class="tab-pane active" id="tab_3_1_content">
                        <div class="c-content-panel">
                          <div class="c-body c-bg-white">
                            <form class="form-horizontal" id="ajaxForm" enctype='multipart/form-data'>
                              {{ csrf_field() }}
                              {{ ajax_action(route('apply_store')) }}
                              {{ ajax_method("post") }}

                              <div class="row">
                                <div class="col-lg-12">

                                  <!-- Father -->
                                  <div class="form-group has-error-father">
                                    <div class="col-md-12">
                                      <label class="control-label">@lang('form.father_bangla.label') *</label>
                                    </div>
                                    <div class="col-md-12">
                                      <input type="text" class="form-control c-square c-theme c-bg-grey"
                                      placeholder="@lang('form.father_bangla.placeholder')" name="father">
                                    </div>
                                  </div>
                                  <!-- Mother -->
                                  <div class="form-group has-error-mother">
                                    <div class="col-md-12">
                                      <label class="control-label">@lang('form.mother_bangla.label') *</label>
                                    </div>
                                    <div class="col-md-12">

                                      <input type="text" class="form-control c-square c-theme c-bg-grey"
                                      placeholder="@lang('form.mother_bangla.placeholder')" name="mother">
                                    </div>
                                  </div>

                                  <div class="c-content-divider c-divider-sm c-left c-bg-grey">
                                    <i class="icon-dot c-bg-grey"></i>
                                  </div>

                                </div>
                              </div>
                              <div id="appendChild">
                                <div id="child-1" class="child">
                                  <div class="row">
                                    <div class="col-lg-8">
                                      <div class="form-group has-error-name-1 has-error-name.1">
                                        <div class="col-md-12">
                                          <label class="control-label">সন্তানের @lang('form.name_bangla.label') *</label>
                                        </div>
                                        <div class="col-md-12">
                                          <input type="text" class="form-control c-square c-theme c-bg-grey"
                                          placeholder="@lang('form.name_bangla.placeholder')" name="name[1]">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-4">
                                      <div class="form-group has-error-age-1">
                                        <div class="col-md-12">
                                          <label class="control-label"> @lang('form.age.label') *</label>
                                        </div>
                                        <div class="col-md-12">
                                          <!--<input type="number" class="form-control c-square c-theme c-bg-grey"-->
                                          <!--placeholder="@lang('form.age.placeholder')" name="age[1]" id="age_1" data-count="1">-->
                                          <select name="age[1]" id="age_1" data-count="1"  class="form-control c-square c-theme c-bg-grey">
                                              <option value="2">-- নির্বাচন করুন --</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                              <option value="9">9</option>
                                              <option value="10">10</option>
                                              <option value="11">11</option>
                                              <option value="12">12</option>
                                              <option value="13">13</option>
                                              <option value="14">14</option>
                                              <option value="15">15</option>
                                            </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                        <div class="col-lg-3">
                                      <div class="form-group has-error-birth_date-1">
                                        <div class="col-md-12">
                                          <label class=" control-label">
                                          <label class=" control-label"> @lang('form.birth.label') * </label>
                                        </div>

                                        <div class="col-md-12">
                                        <input type="text" class="form-control c-square c-theme" placeholder="dd/mm/yyyy" name="birth_date[1]">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group has-error-school">
                                        <div class="col-md-12">
                                          <label class=" control-label"> @lang('form.school.label') </label>
                                        </div>

                                        <div class="col-md-12">
                                          <input type="text" class="form-control c-square c-theme c-bg-grey" placeholder="@lang('form.school.placeholder')" name="school[1]">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-3">
                                      <div class="form-group has-error-class">
                                        <div class="col-md-12">
                                          <label class=" control-label">শ্রেণী </label>
                                        </div>

                                        <div class="col-md-12">
                                        <input type="text" class="form-control c-square c-theme" placeholder="@lang('form.class.placeholder')" name="class[1]">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 c-margin-t-20 c-margin-b-20" style="background-color:#f6ecec">
                                      <h3> কোন প্রতিযোগিতায় অংশ নিতে চান?</h3>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-12">
                                      <h2>১। Art Competition (চিত্র অংকন প্রতিযোগিতা) </h2>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-art">
                                        <div class="col-md-12">
                                          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
                                        </div>
                                        <div class="col-md-12">

                                          <div class="c-checkbox">
                                            <input type="checkbox" id="art_17_1_1" class="c-checkbox" name="art[1][]" value="17_1">
                                            <label for="art_17_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার, সময়: 11AM-12PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="art_17_2_1" name="art[1][]" value="17_2">
                                            <label for="art_17_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন, সময়: 02:30PM – 03:30PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="art_17_3_1" name="art[1][]" value="17_3">
                                            <label for="art_17_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই, সময়: 03:30PM-04:30PM)
                                            </label>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-art">
                                        <div class="col-md-12">
                                          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
                                        </div>
                                        <div class="col-md-12">

                                          <div class="c-checkbox">
                                            <input type="checkbox" id="art_18_1_1" class="c-checkbox" name="art[1][]" value="18_1">
                                            <label for="art_18_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার,  সময়: 11AM-12PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="art_18_2_1" name="art[1][]" value="18_2">
                                            <label for="art_18_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন, সময়: 2PM – 3PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="art_18_3_1" name="art[1][]" value="18_3">
                                            <label for="art_18_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই, সময়: 3PM-4PM)
                                            </label>
                                          </div>


                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <h2>২। Collage Craft Competition (কোলাজ ক্রাফট প্রতিযোগিতা) </h2>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-craft">
                                        <div class="col-md-12">
                                          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
                                        </div>
                                        <div class="col-md-12">

                                          <div class="c-checkbox">
                                            <input type="checkbox" id="craft_17_1_1" class="c-checkbox" name="craft[1][]" value="17_1">
                                            <label for="craft_17_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (থিম: যার যেমন ইচ্ছা,  সময়: 11AM-12PM)
                                            </label>
                                          </div>
                                          <!-- <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="craft_17_2_1" name="craft[1][]" value="17_2">
                                            <label for="craft_17_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর (থিম: যার যেমন ইচ্ছা, সময়: 02:30PM – 03:30PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="craft_17_3_1" name="craft[1][]" value="17_3">
                                            <label for="craft_17_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (থিম: যার যেমন ইচ্ছা, সময়: 03:30PM-04:30PM)
                                            </label>
                                          </div> -->


                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-craft">
                                        <div class="col-md-12">
                                          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
                                        </div>

                                        <div class="col-md-12">

                                          <div class="c-checkbox">
                                            <input type="checkbox" id="craft_18_1_1" class="c-checkbox" name="craft[1][]" value="18_1">
                                            <label for="craft_18_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (থিম: যার যেমন ইচ্ছা,  সময়: 11AM-12PM)
                                            </label>
                                          </div>
                                          <!-- <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="craft_18_2_1" name="craft[1][]" value="18_2">
                                            <label for="craft_18_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর (থিম: যার যেমন ইচ্ছা, সময়: 2PM – 3PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="craft_18_3_1" name="craft[1][]" value="18_3">
                                            <label for="craft_18_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (থিম: যার যেমন ইচ্ছা, সময়: 3PM-4PM)
                                            </label>
                                          </div> -->


                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-12">
                                      <h2>৩। Hand Writing Competition (সুন্দর হাতের লেখা প্রতিযোগিতা)</h2>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-writing">
                                        <div class="col-md-12">
                                          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
                                        </div>
                                        <div class="col-md-12">

                                          <!-- <div class="c-checkbox">
                                            <input type="checkbox" id="writing_17_1_1" class="c-checkbox" name="writing[1][]" value="17_1">
                                            <label for="writing_17_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (সময়: 11AM-12PM)
                                            </label>
                                          </div> -->
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="writing_17_2_1" name="writing[1][]" value="17_2">
                                            <label for="writing_17_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর ( সময়: 02:30PM – 03:30PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="writing_17_3_1" name="writing[1][]" value="17_3">
                                            <label for="writing_17_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (সময়: 03:30PM-04:30PM)
                                            </label>
                                          </div>


                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group has-error-writing">
                                        <div class="col-md-12">
                                          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
                                        </div>
                                        <div class="col-md-12">

                                          <!-- <div class="c-checkbox">
                                            <input type="checkbox" id="writing_18_1_1" class="c-checkbox" name="writing[1][]" value="18_1">
                                            <label for="writing_18_1_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৩ – ৬ বছর (সময়: 11AM-12PM)
                                            </label>
                                          </div> -->
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="writing_18_2_1" name="writing[1][]" value="18_2">
                                            <label for="writing_18_2_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ৭ – ১০ বছর (সময়: 2PM – 3PM)
                                            </label>
                                          </div>
                                          <div class="c-checkbox">
                                            <input type="checkbox" class="c-checkbox" id="writing_18_3_1" name="writing[1][]" value="18_3">
                                            <label for="writing_18_3_1">
                                              <span></span>
                                              <span class="check"></span>
                                              <span class="box"></span>
                                              বয়স: ১১ – ১৫ বছর (সময়: 3PM-4PM)
                                            </label>
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-md-12">
                                  <div class="c-content-divider c-divider-sm c-left c-bg-grey">
                                    <i class="icon-dot c-bg-grey"></i>
                                  </div>
                                </div>

                                <div class="col-md-12 c-margin-b-20">
                                  <button type="button" id="btn-add-more-child" class="btn c-btn-square c-btn-uppercase c-btn-bold btn-fresh"  data-url="{{url('/add/children')}}">একাধিক সন্তানের রেজিস্ট্রেশন করতে (Click Here) <i class="fa fa-plus"></i></button>
                                </div>

                                <div class="col-lg-6">

                                  <div class="form-group has-error-mobile">
                                    <div class="col-md-12">
                                      <label class="control-label">@lang('form.mobile.label') *</label>
                                    </div>

                                    <div class="col-md-12">
                                      <input type="text" class="form-control c-square c-theme c-bg-grey" placeholder="@lang('form.mobile.placeholder')" name="mobile">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group has-error-email">
                                    <div class="col-md-12">
                                      <label class=" control-label">@lang('form.email.label')</label>
                                    </div>

                                    <div class="col-md-12">
                                      <input type="text" class="form-control c-square c-theme c-bg-grey" placeholder="@lang('form.email.placeholder')" name="email">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-lg-12">
                                  <div class="form-group c-margin-t-40">
                                    <div class="col-sm-offset-5 col-md-7">
                                      <button type="submit" class="btn c-btn-square c-btn-uppercase c-btn-bold btn-fresh">Submit Application</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane" id="tab_3_2_content">
                        <div class="c-content-panel">

                          <div class="c-body c-bg-white">
                            <div class="panel-heading">প্রতি বছরের মতো আবারও সকলে আমন্ত্রণ জানাই ফার্ম ফ্রেশ চিলড্রেন’স ডে ১৭-তে।
                              আশা করি প্রতিযোগিতায় অংশগ্রহণকারী সকল সোনামণির বাবা-মা ও অভিভাবককে আমাদের সকল নিয়মাবলি অনুসরণ করবেন।</div>


                              <ul class="c-content-list-1 c-theme c-separator-dot">
                                <li class="c-bg-before-blue">যেকোনো প্রতিযোগিতায় অংশ নিতে অনলাইন রেজিস্ট্রেশন বাধ্যতামূলক। কিন্তু অনুষ্ঠানে অংশ নিতে ও সকল আয়োজনে দিনভর আনন্দ করতে বাবা-মা ও অভিভাবক এবং তাদের সন্তানদের কোন রেজিস্ট্রেশন লাগবে নাহ। এই অনুষ্ঠানটি সকলের জন্য উন্মুক্ত ও ফ্রি।</li>
                                <li class="c-bg-before-blue">Art Competition-এর থিম হলো -  a. বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার), বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন), বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই......)</li>
                                <li class="c-bg-before-blue">Art Competition-এ অংশ নিতে সকল প্রকার কাগজ, রঙ-তুলি-ক্রেয়ন, পেনসিল ইত্যাদি অংশগ্রহণকারীকেই বহন করতে হবে। ফার্ম ফ্রেশ কোনভাবেই এ সকল জিনিস সরবরাহ করবে না ও দায়ভার নিবে না।</li>
                                <li class="c-bg-before-blue">কোলাজ ক্রাফট প্রতিযোগিতায় কোন থিম নেই। যার যেমন ইচ্ছা কোলাজ বানাতে পারবে। সকল প্রকার কাগজ, রঙ-তুলি-ক্রেয়ন, পেনসিল ইত্যাদি অংশগ্রহণকারীকেই বহন করতে হবে। ফার্ম ফ্রেশ কোনভাবেই এ সকল জিনিস সরবরাহ করবে না ও দায়ভার নিবে না।</li>
                                <li class="c-bg-before-blue">সুন্দর হাতের লেখা প্রতিযোগিতায় সকলকে প্যারাগ্রাফ ও লেখার জন্য কাগজ দেওয়া হবে। সেই কাগজে কলম বা পেনসিল যেটাই ব্যাবহার করে লেখা হোক না কেন, সেই পেনসিল বা কলম অংশগ্রহণকারীকেই আনতে হবে। ফার্ম ফ্রেশ কোনভাবেই এ সকল জিনিস সরবরাহ করবে না ও দায়ভার নিবে না।</li>
                                <li class="c-bg-before-blue">সকল প্রতিযোগিতার পুরস্কার ১৮ তারিখ, শনিবার বিকাল ৫টায় ঘোষণা করা হবে।</li>
                              </ul>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>




                </div>
              </div>
            </section>
          </div>
        </div>


        <!-- END: PAGE CONTENT -->

        @endsection

        @section('page_script_plugin')
        <!-- BEGIN: LAYOUT PLUGINS -->
        <script src="/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
        <script src="/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
        <!-- END: LAYOUT PLUGINS -->


        <script src="/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
        @endsection

        @section('page_script')
        <script>
        $(document).ready(function() {
          App.init(); // init core
        });
        </script>
        <!-- END: THEME SCRIPTS -->

        <!-- BEGIN: PAGE SCRIPTS -->
        <script src="/assets/plugins/moment.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="/assets/demos/default/js/scripts/pages/datepicker.js" type="text/javascript"></script>
        <script src="/js/rafiks_js.js"></script>
        <!-- END: PAGE SCRIPTS -->

        <script type="text/javascript">
        $(document).ready(function() {
          global_checkbox_var = 2;

          $('#child-1 input[name^=art]').prop("disabled", true);
          $('#child-1 input[name^=craft]').prop("disabled", true);
          $('#child-1 input[name^=writing]').prop("disabled", true);

          var slider = $('.c-layout-revo-slider .tp-banner');

          var cont = $('.c-layout-revo-slider .tp-banner-container');

          var api = slider.show().revolution({
            sliderType:"standard",
            sliderLayout:"fullscreen",
            fullScreenOffset: '10%',
            dottedOverlay:"none",
            delay:15000,
            navigation: {
              keyboardNavigation:"off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation:"off",
              onHoverStop:"off",
              arrows: {
                style:"circle",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                  h_align:"left",
                  v_align:"center",
                  h_offset:30,
                  v_offset:0
                },
                right: {
                  h_align:"right",
                  v_align:"center",
                  h_offset:30,
                  v_offset:0
                }
              }
            },
            responsiveLevels:[2048,1024,778,480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [868, 768, 960, 720],
            lazyType:"none",
            shadow:0,
            spinner:"spinner2",
            stopLoop:"on",
            stopAfterLoops:0,
            stopAtSlide:1,
            shuffle:"off",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
              simplifyAll:"off",
              nextSlideOnWindowFocus:"off",
              disableFocusListener:false,
            }
          });

          $(".c-content-feature-11 .c-grid > .c-grid-row > li").on('click',function(){
            var route_url = ''+$(this).attr("data-url");
            $(window).attr('location',route_url);
          });

        });
        </script>
        @endsection
