<div class="row">
  <div class="col-md-12">
    <div class="c-content-divider c-divider-sm c-left c-bg-grey">
      <i class="icon-dot c-bg-grey"></i>
    </div>
  </div>
</div>
<div id="child-{{$global}}" class="child">
  <div class="row">
    <div class="col-lg-8">
      <div class="form-group has-error-name-{{$global}}">
        <div class="col-md-12">
          <label class="control-label">সন্তানের @lang('form.name_bangla.label') *</label>
        </div>
        <div class="col-md-12">
          <input type="text" class="form-control c-square c-theme c-bg-grey"
          placeholder="ইংরেজিতে লিখুন" name="name[{{$global}}]">
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="form-group has-error-age-{{$global}}">
        <div class="col-md-12">
          <label class="control-label"> @lang('form.age.label') *</label>
        </div>
        <div class="col-md-12">
          <!--<input type="number" class="form-control c-square c-theme c-bg-grey"-->
          <!--placeholder="@lang('form.age.placeholder')" name="age[{{$global}}]" id="age_{{$global}}" data-count="{{$global}}">-->
            <select  name="age[{{$global}}]" id="age_{{$global}}" data-count="{{$global}}" class="form-control c-square c-theme c-bg-grey">
              <option value="2">-- নির্বাচন করুন --</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
              <option value="7">7</option>
              <option value="8">8</option>
              <option value="9">9</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
            </select>
        </div>
      </div>
    </div>
  </div>
  <div class="row">

     <div class="col-lg-3">
                                      <div class="form-group has-error-birth_date-{{$global}}">
                                        <div class="col-md-12">
                                          <label class=" control-label">
                                          <label class=" control-label"> @lang('form.birth.label') * </label>
                                        </div>

                                        <div class="col-md-12">
                                        <input type="text" class="form-control c-square c-theme" placeholder="dd/mm/yyyy" name="birth_date[{{$global}}]">
                                        </div>
                                      </div>
                                    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-school">
        <div class="col-md-12">
          <label class=" control-label"> @lang('form.school.label') </label>
        </div>

        <div class="col-md-12">
          <input type="text" class="form-control c-square c-theme c-bg-grey" placeholder="ইংরেজিতে লিখুন" name="school[{{$global}}]">
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="form-group has-error-class">
        <div class="col-md-12">
          <label class=" control-label">শ্রেণী </label>
        </div>

        <div class="col-md-12">
        <input type="text" class="form-control c-square c-theme" placeholder="ইংরেজিতে লিখুন" name="class[{{$global}}]">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 c-margin-t-20 c-margin-b-20" style="background-color:#f6ecec">
      <h3> কোন প্রতিযোগিতায় অংশ নিতে চান?</h3>
    </div>
  </div>

  <!--Question 1-->
  <div class="row">
    <div class="col-md-12">
      <h2>১। Art Competition (চিত্র অংকন প্রতিযোগিতা) </h2>
    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-art">
        <div class="col-md-12">
          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
        </div>
        <div class="col-md-12">

          <div class="c-checkbox">
            <input type="checkbox" id="art_17_1_{{$global}}" class="c-checkbox" name="art[{{$global}}][]" value="17_1">
            <label for="art_17_1_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার, সময়: 11AM-12PM)
            </label>
          </div>
          <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="art_17_2_{{$global}}" name="art[{{$global}}][]" value="17_2">
            <label for="art_17_2_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন, সময়: 02:30PM – 03:30PM)
            </label>
          </div>
          <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="art_17_3_{{$global}}" name="art[{{$global}}][]" value="17_3">
            <label for="art_17_3_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই, সময়: 03:30PM-04:30PM)
            </label>
          </div>

        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-art">
        <div class="col-md-12">
          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
        </div>
        <div class="col-md-12">

          <div class="c-checkbox">
            <input type="checkbox" id="art_18_1_{{$global}}" class="c-checkbox" name="art[{{$global}}][]" value="18_1">
            <label for="art_18_1_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার,  সময়: 11AM-12PM)
            </label>
          </div>
          <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="art_18_2_{{$global}}" name="art[{{$global}}][]" value="18_2">
            <label for="art_18_2_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন, সময়: 2PM – 3PM)
            </label>
          </div>
          <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="art_18_3_{{$global}}" name="art[{{$global}}][]" value="18_3">
            <label for="art_18_3_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই, সময়: 3PM-4PM)
            </label>
          </div>


        </div>
      </div>
    </div>
  </div>

  <!--Question 2-->
  <div class="row">
    <div class="col-md-12">
      <h2>২। Collage Craft Competition (কোলাজ ক্রাফট প্রতিযোগিতা) </h2>
    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-craft">
        <div class="col-md-12">
          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
        </div>
        <div class="col-md-12">

          <div class="c-checkbox">
            <input type="checkbox" id="craft_17_1_{{$global}}" class="c-checkbox" name="craft[{{$global}}][]" value="17_1">
            <label for="craft_17_1_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৩ – ৬ বছর (থিম: যার যেমন ইচ্ছা,  সময়: 11AM-12PM)
            </label>
          </div>



        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-craft">
        <div class="col-md-12">
          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
        </div>

        <div class="col-md-12">

          <div class="c-checkbox">
            <input type="checkbox" id="craft_18_1_{{$global}}" class="c-checkbox" name="craft[{{$global}}][]" value="18_1">
            <label for="craft_18_1_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৩ – ৬ বছর (থিম: যার যেমন ইচ্ছা,  সময়: 11AM-12PM)
            </label>
          </div>
          <!-- <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="craft_18_2_{{$global}}" name="craft[{{$global}}][]" value="18_2">
            <label for="craft_18_2_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ৭ – ১০ বছর (থিম: যার যেমন ইচ্ছা, সময়: 2PM – 3PM)
            </label>
          </div>
          <div class="c-checkbox">
            <input type="checkbox" class="c-checkbox" id="craft_18_3_{{$global}}" name="craft[{{$global}}][]" value="18_3">
            <label for="craft_18_3_{{$global}}">
              <span></span>
              <span class="check"></span>
              <span class="box"></span>
              বয়স: ১১ – ১৫ বছর (থিম: যার যেমন ইচ্ছা, সময়: 3PM-4PM)
            </label>
          </div> -->


        </div>
      </div>
    </div>
  </div>
  <!--Question 3-->
  <div class="row">
    <div class="col-md-12">
      <h2>৩। Hand Writing Competition (সুন্দর হাতের লেখা প্রতিযোগিতা)</h2>

    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-writing">
        <div class="col-md-12">
          <label class=" control-label">১ম দিন – ১৭ নভেম্বর (শুক্রবার)</label>
        </div>
        <div class="col-md-12">
          <div class="c-checkbox">
            <!-- <div class="c-checkbox">
              <input type="checkbox" id="writing_17_1_{{$global}}" class="c-checkbox" name="writing[{{$global}}][]" value="17_1">
              <label for="writing_17_1_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ৩ – ৬ বছর (সময়: 11AM-12PM)
              </label>
            </div> -->
            <div class="c-checkbox">
              <input type="checkbox" class="c-checkbox" id="writing_17_2_{{$global}}" name="writing[{{$global}}][]" value="17_2">
              <label for="writing_17_2_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ৭ – ১০ বছর (সময়: 02:30PM-03:30PM)
              </label>
            </div>
            <div class="c-checkbox">
              <input type="checkbox" class="c-checkbox" id="writing_17_3_{{$global}}" name="writing[{{$global}}][]" value="17_3">
              <label for="writing_17_3_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ১১ – ১৫ বছর (সময়: 03:30PM-04:30PM)
              </label>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group has-error-writing">
        <div class="col-md-12">
          <label class=" control-label">২য় দিন – ১৮ নভেম্বর (শনিবার)</label>
        </div>
        <div class="col-md-12">
          <div class="c-checkbox">
            <!-- <div class="c-checkbox">
              <input type="checkbox" id="writing_18_1_{{$global}}" class="c-checkbox" name="writing[{{$global}}][]" value="18_1">
              <label for="writing_18_1_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ৩ – ৬ বছর (সময়: 11AM-12PM)
              </label>
            </div> -->
            <div class="c-checkbox">
              <input type="checkbox" class="c-checkbox" id="writing_18_2_{{$global}}" name="writing[{{$global}}][]" value="18_2">
              <label for="writing_18_2_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ৭ – ১০ বছর (সময়: 2PM – 3PM)
              </label>
            </div>
            <div class="c-checkbox">
              <input type="checkbox" class="c-checkbox" id="writing_18_3_{{$global}}" name="writing[{{$global}}][]" value="18_3">
              <label for="writing_18_3_{{$global}}">
                <span></span>
                <span class="check"></span>
                <span class="box"></span>
                বয়স: ১১ – ১৫ বছর (সময়: 3PM-4PM)
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!---->
