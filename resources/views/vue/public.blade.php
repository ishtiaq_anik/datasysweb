@extends('layouts.master')

@section('title', 'Public')

@section('content')

    <router-view></router-view>

@endsection
