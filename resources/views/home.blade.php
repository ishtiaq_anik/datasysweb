@extends('layouts.master')

@section('title', __('titles.home'))


@section('page_style_plugin')
<!-- BEGIN: BASE PLUGINS  -->
<link href="/assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
<!-- END: BASE PLUGINS -->
@endsection

@section('page_style')
@endsection

@section('content')
<!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
<section class="c-layout-revo-slider c-layout-revo-slider-4" dir="ltr">
    <div class="tp-banner-container c-theme">
        <div class="tp-banner rev_slider" data-version="5.0">
            <ul>
                <!--BEGIN: SLIDE #1 -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                    <img
                    alt=""
                    src="/assets/base/img/content/backgrounds/s19.png"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                    >
                    <div class="tp-caption customin customout"
                    data-x="center"
                    data-y="center"
                    data-hoffset=""
                    data-voffset="-50"
                    data-speed="500"
                    data-start="1000"
                    data-transform_idle="o:1;"
                    data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
                    data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="600">
                    <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
                        @lang('home.slider.title1')
                    </h3>
                </div>
                <div class="tp-caption lft"
                data-x="center"
                data-y="center"
                data-voffset="110"
                data-speed="900"
                data-start="2000"
                data-transform_idle="o:1;"
                data-transform_in="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
                data-transform_out="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;" >
                <a href="{{ route('apply') }}" class="c-action-btn btn btn-lg c-btn-square c-theme-btn c-btn-bold c-btn-uppercase">@lang('home.slider.button-title1')</a>
            </div>
        </li>
        <!--END -->

        <!--BEGIN: SLIDE #2 -->
        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
            <img
            alt=""
            src="/assets/base/img/content/backgrounds/s20.jpg"
            data-bgposition="center center"
            data-bgfit="cover"
            data-bgrepeat="no-repeat"
            >
            <div class="tp-caption customin customout"
            data-x="center"
            data-y="center"
            data-hoffset=""
            data-voffset="-50"
            data-speed="500"
            data-start="1000"
            data-transform_idle="o:1;"
            data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
            data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:600;e:Back.easeInOut;"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.1"
            data-endelementdelay="0.1"
            data-endspeed="600">
            <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
                @lang('home.slider.title2')
            </h3>
        </div>
        <div class="tp-caption lft"
        data-x="center"
        data-y="center"
        data-voffset="110"
        data-speed="900"
        data-start="2000"
        data-transform_idle="o:1;"
        data-transform_in="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;"
        data-transform_out="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;">
        <a href="{{ route('apply') }}" class="c-action-btn btn btn-lg c-btn-square c-theme-btn c-btn-bold c-btn-uppercase">@lang('home.slider.button-title2')</a>
    </div>
</li>
<!--END -->
<!--BEGIN: SLIDE #3 -->
<li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
    <img
    alt=""
    src="/assets/base/img/content/backgrounds/s25.jpg"
    data-bgposition="center center"
    data-bgfit="cover"
    data-bgrepeat="no-repeat"
    >
    <div class="tp-caption customin customout"
    data-x="center"
    data-y="center"
    data-hoffset=""
    data-voffset="-50"
    data-speed="500"
    data-start="1000"
    data-transform_idle="o:1;"
    data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
    data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:600;e:Back.easeInOut;"
    data-splitin="none"
    data-splitout="none"
    data-elementdelay="0.1"
    data-endelementdelay="0.1"
    data-endspeed="600">
    <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
        @lang('home.slider.title1')
    </h3>
</div>
<div class="tp-caption lft"
data-x="center"
data-y="center"
data-voffset="110"
data-speed="900"
data-start="2000"
data-transform_idle="o:1;"
data-transform_in="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;"
data-transform_out="x:100;y:100;rX:120;scaleX:0.75;scaleY:0.75;o:0;s:900;e:Back.easeInOut;">
</div>
</li>
<!--END -->


</ul>
</div>
</div>
</section>
<!-- END: LAYOUT/SLIDERS/REVO-SLIDER-4 -->

<section>
    
    <!-- BEGIN: CONTENT/FEATURES/FEATURES-15-2 -->
    <div id="feature-15-2" class="c-content-feature-15 c-bg-img-center c-bg-grey" >
        <div class="container">

            <div class="row">
                <div class=" col-md-8 col-xs-12">
                    <div class="c-feature-15-container c-bg-white">
                        <h2 class="c-feature-15-title c-font-bold c-font-uppercase c-theme-border c-font-dark">@lang('home.dc.title')</h2>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <img src="/images/dc.jpg" class="c-feature-15-img"/>
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <p class="c-feature-15-desc c-font-dark">
                                    @if(isset($message) && $message)
                                    {{ $message->content }}
                                    @endif
                                </p>
                                <h4 class="c-font-bold">
                                    @if(isset($dc))
                                    {{ $dc->name }}
                                    @endif
                                </h4>
                                <h4 class="c-font-bold c-font-dark">@lang('home.dc.office')</h4>
                                <h4 class="c-font-bold c-font-dark">@lang('home.dc.address')</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-md-4 col-xs-12">
                    <div class=" c-content-feature-15 c-feature-15-container c-bg-white c-padding-10 c-content-title-3 c-title-md c-theme-border">
                        <h2 class="c-feature-15-title c-font-bold c-font-uppercase c-theme-border c-font-dark">@lang('home.dc.notice')</h2>

                        @if($notices->count() > 1)
                        <div class="c-content-media-1-slider" data-slider="owl">
                            <div class="owl-carousel owl-theme c-theme owl-single" data-single-item="true" data-auto-play="8000" data-rtl="false">
                                @foreach($notices as $notice)
                                <div class="item">
                                    <div class="c-content-media-1" data-height="height">
                                        <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"></div>
                                        <!-- <a href="#" class="c-title c-font-uppercase c-font-bold c-theme-on-hover">The Theme of 2015</a> -->
                                        <p class="c-margin-t-20"><a href="#" class=" c-font-uppercase c-font-bold c-theme-on-hover">{{$notice->content}}</a></p>
                                        <div class="c-margin-t-5">
                                            {{date('d M, Y', strtotime($notice->created_at))}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @elseif($notices->count() == 1)
                        <div class="item">
                            <div class="c-content-media-1" data-height="height">
                                <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg"></div>
                                <!-- <a href="#" class="c-title c-font-uppercase c-font-bold c-theme-on-hover">The Theme of 2015</a> -->
                                <p class="c-margin-t-20"><a href="#" class=" c-font-uppercase c-font-bold c-theme-on-hover">{{$notices[0]->content}}</a></p>
                                <div class="c-margin-t-5">
                                    {{date('d M, Y', strtotime($notices[0]->created_at))}}
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- End-->
                    </div>
                    <!-------- End Notice  -------->

                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/FEATURES/FEATURES-15-2 -->
</section>

<section>
    <!-- BEGIN: CONTENT/STEPS/STEPS-1 -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="c-content-title-1 c-margin-b-60">
                        <h3 class="c-center c-font-uppercase c-font-bold">
                            @lang('home.about_service.title')
                        </h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-6 wow animate fadeInLeft">
                    <div class="c-content-step-1 c-opt-1">
                        <div class="c-icon">
                            <span class="c-hr c-hr-first"><span class="c-content-line-icon c-icon-11 c-theme"></span></span>
                        </div>
                        <div class="c-title c-font-20 c-font-bold c-font-uppercase">
                            @lang('home.about_service.one.title')
                        </div>
                        <div class="c-description c-font-17">
                            @lang('home.about_service.one.content')
                        </div>
                        <a href="{{ route('apply') }}" class="btn c-btn-square c-theme-btn c-btn-border1-2x c-btn-uppercase c-btn-bold">@lang('home.about_service.explore')</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 wow animate fadeInLeft" data-wow-delay="0.2s">
                    <div class="c-content-step-1 c-opt-1">
                        <div class="c-icon"><span class="c-hr"><span class="c-content-line-icon c-icon-21 c-theme"></span></span></div>
                        <div class="c-title c-font-20 c-font-bold c-font-uppercase">
                            @lang('home.about_service.two.title')
                        </div>
                        <div class="c-description c-font-17">
                            @lang('home.about_service.two.content')
                        </div>
                        <a href="{{ route('check') }}" class="btn c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">@lang('home.about_service.explore')</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 wow animate fadeInLeft" data-wow-delay="0.4s">
                    <div class="c-content-step-1 c-opt-1">
                        <div class="c-icon"><span class="c-hr c-hr-last"><span class="c-content-line-icon c-icon-32 c-theme"></span></span></div>
                        <div class="c-title c-font-20 c-font-bold c-font-uppercase">
                            @lang('home.about_service.three.title')
                        </div>
                        <div class="c-description c-font-17">
                            @lang('home.about_service.three.content')
                        </div>
                        <a href="{{ route('location') }}" class="btn c-btn-square c-theme-btn c-btn-uppercase c-btn-bold">@lang('home.about_service.explore')</a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- END: CONTENT/STEPS/STEPS-1 -->
</section>

<section>
    <!-- BEGIN: CONTENT/BARS/BAR-3 -->
    <div class="c-content-box c-size-md c-bg-dark">
        <div class="container">
            <div class="c-content-bar-3">
                <div class="row">
                    <div class="col-md-7">
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">@lang('home.apply.title')</h3>
                            <p class="c-font-uppercase">@lang('home.apply.message')</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <div class="c-content-v-center" style="height: 90px;">
                            <div class="c-wrapper">
                                <div class="c-body">
                                    <a href="{{ route('apply') }}" class="btn btn-xlg c-btn-blue c-font-uppercase c-font-bold c-btn-square c-btn-border-1x">@lang('home.apply.apply_now')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: CONTENT/BARS/BAR-3 -->
</section>

<section>
    <!-- BEGIN: CONTENT/MISC/SERVICES-4 -->
    <div class="c-content-box c-size-md c-bg-img-top c-bg-parallax" style="background-image: url(../../assets/base/img/content/backgrounds/bg-63.jpg)">
        <div class="container">
            <div class="c-content-feature-11">
                <div class="row">
                    <div class="col-md-6 c-grid" data-auto-height="true" data-related="#c-video-card-3">
                        <ul class="c-grid-row">
                            <li data-url="{{route('apply')}}">
                                <div class="c-bg-opacity-1 c-card" data-height="height">
                                    <h3 class="c-font-uppercase c-font-bold">@lang('home.service.one.title')</h3>
                                    <a href="#" class="btn c-btn-dark-2 c-btn-uppercase c-btn-bold c-btn-border-2x">@lang('home.service.one.button')</a>
                                    <!-- <ul class="c-content-list-1 c-theme c-separator-dot c-square">
                                    <li class="c-bg-before-red">@lang('home.service.one.subtitle1')</li>
                                    <li class="c-bg-before-blue">@lang('home.service.one.subtitle2')</li>
                                    <li class="c-bg-before-green">@lang('home.service.one.subtitle3')</li>
                                    <li class="c-bg-before-purple">@lang('home.service.one.subtitle4')</li>
                                </ul> -->
                            </div>
                        </li>
                        <li data-url="{{route('lost')}}">
                            <div class="c-bg-opacity-1 c-card" data-height="height" >
                                <h3 class="c-font-uppercase c-font-bold">@lang('home.service.two.title')</h3>
                                <a href="#" class="btn c-btn-dark-2 c-btn-uppercase c-btn-bold c-btn-border-2x">@lang('home.service.two.button')</a>
                                <!-- <ul class="c-content-list-1 c-theme c-separator-dot c-square">
                                <li class="c-bg-before-red">@lang('home.service.two.subtitle1')</li>
                                <li class="c-bg-before-blue">@lang('home.service.two.subtitle2')</li>
                                <li class="c-bg-before-green">@lang('home.service.two.subtitle3')</li>
                                <li class="c-bg-before-purple">@lang('home.service.two.subtitle4')</li>
                            </ul> -->
                        </div>
                    </li>
                </ul>
                <ul class="c-grid-row">
                    <li data-url="{{route('verify')}}">
                        <div class="c-bg-opacity-1 c-card" data-height="height">
                            <h3 class="c-font-uppercase c-font-bold">@lang('home.service.three.title')</h3>
                            <a href="#" class="btn c-btn-dark-2 c-btn-uppercase c-btn-bold c-btn-border-2x">@lang('home.service.three.button')</a>
                            <!-- <p>
                            @lang('home.service.three.subtitle')
                        </p> -->
                    </div>
                </li>
                <li data-url="{{route('check')}}">
                    <div class="c-bg-opacity-1 c-card" data-height="height">
                        <h3 class="c-font-uppercase c-font-bold">@lang('home.service.four.title')</h3>
                        <a href="#" class="btn c-btn-dark-2 c-btn-uppercase c-btn-bold c-btn-border-2x">@lang('home.service.four.button')</a>

                        <!-- <p>
                        @lang('home.service.four.subtitle')
                    </p> -->
                </div>
            </li>
        </ul>
    </div>
    <div class="col-md-6 c-video">
        <div id="c-video-card-3" class="embed-responsive embed-responsive-16by9">
            <!--
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/75608357?title=0&byline=0&portrait=0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        -->
        @lang('home.certificate_image.img')
    </div>
</div>
</div>
</div>
</div>
</div><!-- END: CONTENT/MISC/SERVICES-4 -->

</section>
@endsection

@section('page_script_plugin')
<!-- BEGIN: LAYOUT PLUGINS -->
<script src="/assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="/assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="/assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="/assets/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="/assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->
@endsection

@section('page_script')
<script>
$(document).ready(function() {
    App.init(); // init core
});
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script>
$(document).ready(function() {

    var slider = $('.c-layout-revo-slider .tp-banner');
    var cont = $('.c-layout-revo-slider .tp-banner-container');
    var height = (App.getViewPort().width < App.getBreakpoint('md') ? 400 : 620);

    var api = slider.show().revolution({
        sliderType:"standard",
        sliderLayout:"fullwidth",
        delay: 15000,
        autoHeight: 'off',
        gridheight:500,

        navigation: {
            keyboardNavigation:"off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation:"off",
            onHoverStop:"on",
            arrows: {
                style:"circle",
                enable:true,
                hide_onmobile:false,
                hide_onleave:false,
                tmp:'',
                left: {
                    h_align:"left",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                },
                right: {
                    h_align:"right",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                }
            },
            touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
            },
        },
        viewPort: {
            enable:true,
            outof:"pause",
            visible_area:"80%"
        },

        shadow: 0,

        spinner: "spinner2",

        disableProgressBar:"on",

        fullScreenOffsetContainer: '.tp-banner-container',

        hideThumbsOnMobile: "on",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "on",
        hideArrowsOnMobile: "on",
        hideThumbsUnderResolution: 0,

    });
    $(".c-content-feature-11 .c-grid > .c-grid-row > li").on('click',function(){
        var route_url = ''+$(this).attr("data-url");
        $(window).attr('location',route_url);
    });
}); //ready
</script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
@endsection
