@extends('layouts.master')

@section('title', __('titles.success.title'))



@section('page_style_plugin')
<!-- BEGIN: PAGE STYLES -->
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END: PAGE STYLES -->
@endsection

@section('page_style')
@endsection

@section('content')

<div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center" style="background-image: url(/assets/base/img/content/backgrounds/cover.png)">
    <div class="container">
        <div class="c-content-title-1">
            <h3 class="c-center c-font-uppercase c-font-bold c-font-white">@lang('titles.success.title')</h3>
            <div class="c-line-center c-theme-bg"></div>
            <p class="c-center">@lang('titles.success.subtitle')</p>
        </div>
    </div>
</div>

<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-box c-size-md c-bg-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="c-content-panel">
                    <div class="c-body">
                        <p>{{ $message }}<a href="{{route('apply')}}"> Clck Here<span class="c-arrow c-toggler"></span> to go back</a></p>
                        <p>একটি এস এম এস আসবে, এস.এম.এস-টি সংরক্ষণ করে রাখবেন।
                            এস. এম. এস. না আসলে ধরে নিতে হবে আপনার রেজিস্ট্রেশন সফল হয় নি, আবার করুন।
                            </p>
                            <p>প্রতি বছরের মতো আবারও সকলে আমন্ত্রণ জানাই ফার্ম ফ্রেশ চিলড্রেন’স ডে ১৭-তে। আশা করি প্রতিযোগিতায় অংশগ্রহণকারী সকল সোনামণির বাবা-মা ও অভিভাবককে আমাদের সকল নিয়মাবলি অনুসরণ করবেন।</p>
                            <ul class="c-content-list-1 c-theme c-separator-dot">
                              <li class="c-bg-before-purple"><strong>নিম্নের তথ্যগুলো মনে রাখার অনুরোধ জানাচ্ছিঃ</strong></li>
                              <li class="c-bg-before-red">১। যেকোনো প্রতিযোগিতায় অংশ নিতে অনলাইন রেজিস্ট্রেশন বাধ্যতামূলক। কিন্তু অনুষ্ঠানে অংশ নিতে ও সকল আয়োজনে দিনভর আনন্দ করতে বাবা-মা ও অভিভাবক এবং তাদের সন্তানদের কোন রেজিস্ট্রেশন লাগবে নাহ। এই অনুষ্ঠানটি সকলের জন্য উন্মুক্ত ও ফ্রি।</li>
                              <li class="c-bg-before-red">২। Art Competition-এর থিম হলো -  a. বয়স: ৩ – ৬ বছর (থিম: আমি ও আমার পরিবার), বয়স: ৭ – ১০ বছর (থিম: আমার সবচেয়ে প্রিয় দিন), বয়স: ১১ – ১৫ বছর (থিম: বড় হয়ে আমি হতে চাই......)</li>
                              <li class="c-bg-before-red">৩। Art Competition-এ অংশ নিতে সকল প্রকার কাগজ, রঙ-তুলি-ক্রেয়ন</li>
                           </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END: PAGE CONTENT -->

    @endsection

    @section('page_script_plugin')
    <script src="/assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="/assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
    @endsection

    @section('page_script')
    <script>
    $(document).ready(function() {
        App.init(); // init core
    });
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="/assets/plugins/moment.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/assets/demos/default/js/scripts/pages/datepicker.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<script>


</script>
</script>
@endsection
