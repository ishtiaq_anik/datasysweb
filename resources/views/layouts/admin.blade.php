<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include ('layouts.meta-content')
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700|Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="{{ asset('vendor/bootstrap-3.3.7/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/font-awesome-4.7.0/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/nprogress/nprogress.css') }}" rel="stylesheet">
        @yield('style_main')
        <link href="{{ asset('vendor/metronic/css/components.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/metronic/css/plugins.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/metronic/css/layout.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/metronic/css/themes/light.css') }}" rel="stylesheet">
        @yield('style')


    </head>
    <body class="page-container-bg-solid page-header-fixed page-footer-fixed page-sidebar-closed-hide-logo">
        @include ('admin.master-header')
        <div class="clearfix"> </div>
        <div class="page-container">
            
            @yield('content')
        </div>
        @include ('admin.master-footer')
        <script src="{{ asset('vendor/jquery-2.2.4.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/bootstrap-3.3.7/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/nprogress/nprogress.js')}}" type="text/javascript"></script>
        @yield('script_main')
        <script src="{{ asset('vendor/metronic/js/app.js') }}" type="text/javascript"></script>
        <script src="{{ asset('vendor/metronic/js/layout.js') }}" type="text/javascript"></script>
        @yield('script')
    </body>
</html>
