<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ config('app.locale') }}">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include ('layouts.meta-content')

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="/assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    @yield('page_style_plugin')

    <link href="/assets/plugins/nprogress/nprogress.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/demos/default/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/demos/default/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="/assets/demos/default/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
    <link href="/css/custom.css?ver=1.1.1" rel="stylesheet" type="text/css"/>

    @yield('page_style')

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109274862-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109274862-1');
</script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen c-layout-header-topbar c-bg-grey">
    <script src="/assets/plugins/nprogress/nprogress.js" type="text/javascript"></script>
    <script>NProgress.start();</script>
    <!-- BEGIN: HEADER -->
    <header class="c-layout-header c-layout-header-default c-layout-header-dark-mobile c-header-transparent-light" data-minimize-offset="80">
        <div class="c-navbar">
            <div class="container">
                <!-- BEGIN: BRAND -->
                <div class="c-navbar-wrapper clearfix">


                    <!-- Dropdown menu toggle on mobile: c-toggler class can be applied to the link arrow or link itself depending on toggle mode -->
                    <nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold">
                        <ul class="nav navbar-nav c-theme-nav">
                            <li class="c-onepage-link " >
                                <a href="#tab_3_1_content" class="c-link dropdown-toggle">Apply Now<span class="c-arrow c-toggler"></span></a>
                            </li>
                            <li class="c-onepage-link ">
                                <a href="#rules" class="c-link dropdown-toggle">Rules & Registration<span class="c-arrow c-toggler"></span></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/farmfreshuhtmilk/" class="c-link dropdown-toggle">About Us<span class="c-arrow c-toggler"></span></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/events/189543991598314/" class="c-link dropdown-toggle">Facebook Event<span class="c-arrow c-toggler"></span></a>
                            </li>

                        </ul>
                    </nav>
                    <!-- END: MEGA MENU --><!-- END: LAYOUT/HEADERS/MEGA-MENU -->
                    <!-- END: HOR NAV -->
                </div>
                <!-- BEGIN: LAYOUT/HEADERS/QUICK-CART -->

            </div>
        </div>
    </header>
    <!-- END: HEADER --><!-- END: LAYOUT/HEADERS/HEADER-1 -->

    <div class="c-layout-page">
        @yield('content')
    </div>
    @include ('layouts.footer')

    <!-- BEGIN: LAYOUT/BASE/BOTTOM -->
    <!-- BEGIN: CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="/assets/plugins/excanvas.min.js"></script>
    <![endif]-->

    <script src="/assets/plugins/jquery.min.js" type="text/javascript" ></script>
    <script src="/assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
    <script src="/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
    <script src="/assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
    <script src="/assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
    <script src="/assets/demos/default/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>
    <!-- END: CORE PLUGINS -->

    @yield('page_script_plugin')

    <!-- BEGIN: THEME SCRIPTS -->
    <script src="/assets/base/js/components.js" type="text/javascript"></script>
    <script src="/assets/base/js/components-shop.js" type="text/javascript"></script>
    <script src="/assets/base/js/app.js" type="text/javascript"></script>
    <script src="/js/custom.js?ver=1.1.2" type="text/javascript"></script>
    <script src="http://malsup.github.io/jquery.blockUI.js" type="text/javascript"></script>

    @yield('page_script')
</body>
<!-- END: PAGE CONTAINER -->
</body>
</html>
