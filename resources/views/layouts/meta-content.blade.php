<meta charset="UTF-8">
<meta name="keywords" content="">
<meta name="author" content="Datasys Team">
<meta name="description" content="">
<meta name="subject" content="">
<meta name="url" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>@yield('title') - {{ __('titles.application') }} </title>

<link href="{{ asset('favicon.png') }}" rel="shortcut icon"/>
