<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-5 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-1 c-layout-footer-3 c-bg-dark">

    <div class="c-postfooter">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 c-col c-center">
                    <p class="c-copyright c-font-grey-3"> &copy; 2017 All Rights Reserved By
                        <span class="c-font-grey">Akij Group</span>
                    </p>
                </div>
                <div class="col-md-4 col-sm-6 c-col c-center">
                    <p class="c-copyright c-font-grey-3">Developed By
                        <span class="c-font-grey"> Datacraft Ltd</span>
                    </p>
                </div>
                <div class="col-md-4 col-sm-12 c-col c-center">
                    <p class="c-copyright c-font-grey-3">Overall Collaboration
                        <span class="c-font-grey">Mediacom Bd</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END: LAYOUT/FOOTERS/FOOTER-5 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END: LAYOUT/FOOTERS/GO2TOP -->
