<div class="page-footer">
    <div class="page-footer-inner">
        Copyright &copy; {{Config::get('app.name')}} {{date('Y')}}
        <a target="_blank" href="{{Config::get('app.author_url')}}"> {{Config::get('app.author')}} </a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>