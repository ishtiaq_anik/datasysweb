<?php

return [

    'home' => "হোম",
    'certificate' => 'সনদ',
    'certificates' => [
        'new' => 'নতুন সনদ',
        'lost' => 'হারানো সনদ',
        'check' => 'সনদের অবস্থা দেখুন',
        'location' => 'সনদ প্রাপ্তি স্থান'
    ],
    'verification' => 'যাচাইকরণ',
    'faq' => "প্রায়শই জিজ্ঞাসিত প্রশ্ন",
    "contact" => "যোগাযোগ",
    "language" => [
        'id' => "en",
        'name' => "English",
        'src' => "/images/gb.png",
    ],
];
