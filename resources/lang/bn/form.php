<?php

return [
    'next'   => 'পরবর্তী',
    'prev'   => 'পূর্ববর্তী',
    'submit' => 'প্রেরণ করুন',
    'preview' => 'নিরীক্ষণ করুন',
    'cancel' => 'বাতিল করুন',
    'preferred_lang' => [
        'label' => 'সনদের ভাষা',
        'bangla' => 'বাংলা',
        'english' => 'ইংরেজি',
    ],
    'type' => [
        'label' => 'উপজাতি',
        'tribal' => 'হ্যাঁ',
        'nontribal' => 'না',
    ],
    'name_bangla' => [
        'label' => 'নাম',
        'placeholder' => 'নাম',
    ],
    'name' => [
        'label' => 'Name',
        'placeholder' => 'Name',
    ],
    'birth' => [
        'label' => 'জন্ম তারিখ',
    ],


    'mobile' => [
        'label' => 'মোবাইল নম্বর',
        'placeholder' => 'মোবাইল নম্বর',
    ],
    'email' => [
        'label' => 'ইমেইল (ঐচ্ছিক)',
        'placeholder' => 'উদাঃ youremail@domain',
    ],
    'father' => [
        'label' => 'Father\'s Name',
        'placeholder' => 'Father\'s Name',
    ],
    'father_bangla' => [
        'label' => 'বাবার নাম',
        'placeholder' => 'বাবার নাম',
    ],

    'mother_bangla' => [
        'label' => 'মাতার নাম',
        'placeholder' => 'মাতার নাম',
    ],


    'age' => [
        'label' => 'বয়স',
        'placeholder' => 'বয়স',
    ],
    'school' => [
        'label' => 'স্কুলের নাম (যদি কোনও স্কুলে ভর্তি হয়)',
        'placeholder' => 'স্কুলের নাম',
    ],
    'class' => [
        'label' => 'শ্রেণী',
        'placeholder' => 'শ্রেণী',
    ],
    'apply_participant_message'     => 'প্রতিযোগিতার জন্য আবেদন সফলভাবে সম্পন্ন হয়েছে',
    'contact_default'               => 'বার্তা পাঠানো হল',
    'contact_message'               => 'বার্তা সফলভাবে পাঠানো হল',
    'add_more_child'                => 'একাধিক সন্তানের নিবন্ধন করতে (Click Here)',
];
?>
