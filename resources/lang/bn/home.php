<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'slider' =>[
        'title1' => 'স্থায়ী বাসিন্দা সনদের জন্য <br> অনলাইনে আবেদন করুন',
        'button-title1'=>'আবেদন করুন',
        'title2' => 'স্থায়ী বাসিন্দা সনদের জন্য <br> অনলাইনে আবেদন করুন',
        'button-title2'=>'আবেদন করুন'

    ],

    'apply' =>[
        'title' => 'স্থায়ী বাসিন্দা সনদ এর জন্য নিবন্ধন করুন',
        'subtitle' => 'স্থায়ী বাসিন্দা সনদ এর জন্য নিবন্ধন করুন',
        'button-title' => 'নিবন্ধন করুন'
    ],

    'dc' =>[
        'title' => 'জেলা প্রশাসকের বার্তা',
        'notice' => 'বিজ্ঞপ্তি সমূহ',
        'office' =>'জেলা প্রশাসক',//ের কার্যালয়
        'address' => 'রাংগামাটি পার্বত্য জেলা।'
    ],

    'service' =>[
        'header' => [
            'title'=>'আমাদের সেবা সমূহ',
        ],
        'one' => [
            'title'=>'নতুন সনদের জন্য আবেদন',
            'button'=>'আবেদন করুন',
            'subtitle1'=>'পাসর্পোট সাইজ আকারের ০১ (এক) কপি রঙ্গিন ছবি।',
            'subtitle2'=>'অনলাইন জন্ম নিবন্ধনের কপি।',
            'subtitle3'=>'পৌর মেয়র/ইউপি চেয়ারম্যান প্রদত্ত জাতীয়তা সনদপত্রি।',
            'subtitle4'=>'অন্যান্য (যদি থাক) যেমন-জাতীয় পরিচয়পত্র।',

        ],
        'two' => [
            'title'=>'হারানো সনদ',
            'button'=>'আবেদন করুন',
            'subtitle1'=>'জিডির কপি।',
            'subtitle2'=>'অনলাইন জন্ম নিবন্ধনের নম্বর।',
            'subtitle3'=>'হারানো সনদের নম্বর।',
            'subtitle4'=>'জাতীয় পরিচয়পত্র নম্বর (যদি থাকে)।',
        ],
        'three' => [
            'title'=>'সনদের অবস্থা পর্যবেক্ষণ করুন',
            'button'=>'পর্যবেক্ষণ করুন',
            'subtitle'=>'আপনার মোবাইলে প্রেরণকৃত ট্র্যাকিং আইডি।',
        ],
        'four' => [
            'title'=>'সনদটি সঠিক কিনা যাচাই করুন',
            'button'=>'যাচাই করুন',
            'subtitle'=>'যে সনদটি যাচাই করতে চান সেই সনদের নম্বর।',
        ],

    ],

    'certificate_image' =>[
        'img' => '<img alt="" height="325px" width="100%" src="/assets/base/img/content/certificates/certificate_bn.jpg">',
    ],

    'apply' =>[
        'apply_now' => 'আবেদন করুন',
        'title'     => 'আপনি কি স্থায়ী বাসিন্দা সনদের জন্য আবেদন করতে চান?',
        'message'   => 'স্থায়ী বাসিন্দা সনদের জন্য আবেদন এখন আরো সহজ। এখনই আবেদন করুন।',
    ],

    'about_service' =>[
        'title'     => 'পদ্ধতি সম্পর্কে',
        'sub_title' => 'পদ্ধতি সম্পর্কে জানুন',
        'explore'   => 'অনুসন্ধান করুন',
        'contact_us'=> 'যোগাযোগ করুন',
        'one' => [
            'title'     => 'নতুন সনদের জন্য আবেদন',
            'content'   => 'আপনাকে ফর্ম পূরণের মাধ্যমে একটি নতুন সনদের জন্য আবেদন করতে হবে।',
        ],
        'two' => [
            'title'     => 'অগ্রগতির অবস্থা পর্যালোচনা করুন',
            'content'   => 'একটি নতুন সনদের জন্য আবেদন করার পর আপনি অগ্রগতি পর্যালোচনা করতে পারেন।',
        ],
        'three' => [
            'title'     => 'সনদ সংগ্রহ',
            'content'   => 'আপনি ইউডিসি / ডিসি অফিস থেকে আপনার সনদের সংগ্রহ করতে পারেন।',
        ],
    ],
    'footer' => [
        'useful_link' => [
            'title' => 'প্রয়োজনীয় লিংকসমূহ',
            'ministry_of_public_administration' => 'জনপ্রশাসন মন্ত্রণালয়',
            'national_web_portal' => 'বাংলাদেশ জাতীয় তথ্য বাতায়ন',
            'prime_minister_office' => 'প্রধানমন্ত্রীর কার্যালয়',
            'bangladesh_tourism_board' => 'বাংলাদেশ পর্যটন বোর্ড',
            'hilltracts_development_board' => 'হিলট্র্যাক্টস ডেভেলপমেন্ট বোর্ড',
            'hilltracts_regional_council' => 'পার্বত্য চট্টগ্রাম আঞ্চলিক পরিষদ',
        ],
        'find_us' => [
            'title' => 'আমাদের সন্ধান',
        ],
        'dc_office' => [
            'title' => 'জেলা প্রশাসকের কার্যালয়',
        ],
    ],
];
