<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'আপনার :attribute অবশ্যই :min সংখ্যার হতে হবে ',
    //    'string'  => 'Th :attribute must be at least fffsf :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'একটি অন্তত নির্বাচন আবশ্যক',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'এই :attribute টি আগেই নেওয়া হয়েছে ।',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [

        'preferred_lang' => [
            'required' => 'আপনার সনদের ভাষা নির্বাচন করুন',
        ],
        'type' => [
            'required' => 'আপনার উপজাতি নির্বাচন করুন',
        ],
        'name_bangla' => [
            'required' => 'আপনার সম্পূর্ণ নাম লিখুন',
        ],
        'name' => [
            'required' => 'আপনার সম্পূর্ণ নাম ইংরেজি বড় অক্ষয়রে লিখুন',
        ],
        'birth_date' => [
            'required' => 'আপনার জন্মের তারিখ নির্বাচন করুন',
        ],
        'gender' => [
            'required' => 'আপনার লিঙ্গ নির্বাচন করুন',
        ],
        'marital_status' => [
            'required' => 'আপনার বৈবাহিক অবস্থা নির্বাচন করুন',
        ],
        'mobile' => [
            'required' => 'আপনার মোবাইল নম্বর লিখুন',
        ],
        'email' => [
            'required' => 'আপনার ইমেইল লিখুন',
        ],
        'father' => [
            'required' => 'আপনার পিতার নাম লিখুন',
        ],
        'husband' => [
            'required' => 'আপনার স্বামীর নাম লিখুন',
        ],
        'mother' => [
            'required' => 'আপনার মাতার নাম লিখুন',
        ],
        'father_bangla' => [
            'required' => 'আপনার পিতার নাম লিখুন',
        ],
        'husband_bangla' => [
            'required' => 'আপনার স্বামীর নাম লিখুন',
        ],
        'mother_bangla' => [
            'required' => 'আপনার মাতার নাম লিখুন',
        ],
        'village' => [
            'required' => 'আপনার গ্রামের নাম লিখুন',
        ],
        'ward' => [
            'required' => 'আপনার ওয়ার্ড নির্বাচন করুন',
        ],
        'union' => [
            'required' => 'আপনার ইউনিয়ন নির্বাচন করুন',
        ],
        'post' => [
            'required' => 'আপনার পোস্ট অফিস নির্বাচন করুন',
        ],
        'postcode' => [
            'required' => 'আপনার পোস্টপোস্ট কোড নির্বাচন করুন',
        ],
        'upazila' => [
            'required' => 'আপনার উপজিলা নির্বাচন করুন',
        ],
        'picture' => [
            'required' => 'আপনার নিজের ছবি আপলোড করুন',
        ],
        'birth_reg_no' => [
            'required' => 'আপনার জন্ম নিবন্ধন নম্বর লিখুন',
        ],
        'birth_file' => [
            'required' => 'জন্ম নিবন্ধনের স্ক্যান করা অনুলিপি আপলোড করুন',
        ],
        'gd_file' => [
            'required' => 'জিডির স্ক্যান করা অনুলিপি আপলোড করুন',
        ],
        'nid' => [
            'required' => 'জাতীয় পরিচয়পত্রের নম্বর লিখুন',
        ],
        'nid_file' => [
            'required' => 'জাতীয় পরিচয়পত্রের স্ক্যান করা অনুলিপি আপলোড করুন',
        ],
        'chairman_mayor' => [
            'required' => 'চেয়ারম্যান / মেয়র এর সনদের স্ক্যান করা অনুলিপি আপলোড করুন',
        ],
        'delivery_point' => [
            'required' => 'সার্টিফিকেট সংগ্রহের স্থান নির্বাচন করুন',
        ],
        'check_id' => [
            'required' => 'আপনার মোবাইল/ট্র্যাকিং নম্বরটি লিখুন ',
        ],
        'certificate_number' =>  [
            'required' => 'আপনার সনদ নম্বরটি লিখুন ',
            'unique'   => 'এই সনদটি ইতিমধ্যে গ্রহণ করা হয়েছে',
        ],
        'birth_reg_no' =>  [
            'required' => 'আপনার জন্ম নিবন্ধন নম্বর নম্বরটি লিখুন ',
            'unique'   => 'এই জন্ম নিবন্ধন নম্বর নম্বরটি ইতিমধ্যে গ্রহণ করা হয়েছে',
        ],
        'age' =>  [
            'required' => 'আপনার জন্ম নিবন্ধন নম্বর নম্বরটি লিখুন ',
            'unique'   => 'এই জন্ম নিবন্ধন নম্বর নম্বরটি ইতিমধ্যে গ্রহণ করা হয়েছে',
        ],
        'school' =>  [
            'required' => 'আপনার জন্ম নিবন্ধন নম্বর নম্বরটি লিখুন ',
            'unique'   => 'এই জন্ম নিবন্ধন নম্বর নম্বরটি ইতিমধ্যে গ্রহণ করা হয়েছে',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
