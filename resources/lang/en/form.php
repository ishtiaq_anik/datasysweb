<?php

return [
    'next'   => 'Next',
    'prev'   => 'Prev',
    'submit' => 'Submit',
    'preview' => 'Preview',
    'cancel' => 'Cancel',
    'preferred_lang' => [
        'label' => 'Preferred Language',
        'bangla' => 'Bangla',
        'english' => 'English',
    ],
    'type' => [
        'label' => 'Tribal',
        'tribal' => 'Yes',
        'nontribal' => 'No',
    ],
    'name_bangla' => [
        'label' => 'নাম',
        'placeholder' => 'নাম',
    ],
    'name' => [
        'label' => 'Name',
        'placeholder' => 'Name',
    ],
    'birth' => [
        'label' => 'Date of Birth',
    ],
    'gender' => [
        'label' => 'Gender',
        'male' => 'Male',
        'female' => 'Female',
    ],
    'marital_status' => [
        'label' => 'Marital Status',
        'married' => 'Married',
        'unmarried' => 'Unmarried',
    ],
    'mobile' => [
        'label' => 'Mobile Number',
        'placeholder' => 'Mobile Number',
    ],
    'email' => [
        'label' => 'Email Address',
        'placeholder' => 'Ex. youremail@domain',
    ],
    'father' => [
        'label' => 'Father\'s Name',
        'placeholder' => 'Father\'s Name',
    ],
    'father_bangla' => [
        'label' => 'বাবার নাম',
        'placeholder' => 'বাবার নাম',
    ],
    'father' => [
        'label' => 'Father\'s Name',
        'placeholder' => 'Father\'s Name',
    ],
    'father_bangla' => [
        'label' => 'বাবার নাম',
        'placeholder' => 'বাবার নাম',
    ],
    'husband' => [
        'label' => 'Husband\'s Name',
        'placeholder' => 'Husband\'s Name',
    ],
    'husband_bangla' => [
        'label' => 'স্বামীর নাম',
        'placeholder' => 'স্বামীর নাম',
    ],
    'mother' => [
        'label' => 'Mother\'s Name',
        'placeholder' => 'Mother\'s Name',
    ],
    'mother_bangla' => [
        'label' => 'মাতার নাম',
        'placeholder' => 'মাতার নাম',
    ],
    'upazila' => [
        'label' => 'Upazila',
        'placeholder' => 'Upazila',
        'select' => 'Select Upazila',
    ],
    'union' => [
        'label' => 'Union / Pourashava',
        'placeholder' => 'Union / Pourashava',
        'select' => 'Select Union',
    ],
    'tribal' => [
        'label' => 'Tribal Type',
        'placeholder' => 'Tribal Type',
        'select' => 'Select Tribal Type',
    ],
    'postoffice' => [
        'label' => 'Post Office',
        'placeholder' => 'Post Office',
        'select' => 'Select Post Office',
    ],
    'postcode' => [
        'label' => 'Postcode',
        'placeholder' => 'Postcode',
        'select' => 'Select Post Code',
    ],
    'ward' => [
        'label' => 'Ward',
        'placeholder' => 'Ward',
        'select' => 'Select Ward',
    ],
    'village' => [
        'label' => 'Village',
        'placeholder' => 'Village',
    ],
    'picture' => [
        'label' => 'Picture',
        'select' => 'Select File',
        'change' => 'Change',
        'remove' => 'Remove',
        'help' => 'Upload your picture',
    ],
    'birth_reg_no' => [
        'label' => 'Birth Registration Number',
        'placeholder' => 'Birth Registration Number',
    ],
    'birth_file' => [
        'label' => 'Birth Registration Number File',
        'select' => 'Select File',
        'change' => 'Change',
        'remove' => 'Remove',
        'help' => 'Upload the scanned copy of Birth Certificate',
    ],
    'nid' => [
        'label' => 'National ID',
        'placeholder' => 'National ID',
    ],
    'nid_file' => [
        'label' => 'National ID Scanned Copy',
        'select' => 'Select File',
        'change' => 'Change',
        'remove' => 'Remove',
        'help' => 'Upload the scanned copy of National ID',
    ],
    'chairman_mayor' => [
        'label' => 'UP Chairman / Mayor\'s Certificate',
        'select' => 'Select File',
        'change' => 'Change',
        'remove' => 'Remove',
        'help' => 'Upload the scanned copy of Chairman / Mayor certificate',
    ],
    'delivery_point' => [
        'label' => 'Certificate Collection Place',
        'select' => '-- Select Place --',
    ],

    'lost' => [
        'label' => 'GD Copy Scanned File',
        'select' => 'GD Copy Scanned',
        'change' => 'Change',
        'remove' => 'Remove',
        'help' => 'Upload GD Copy Scanned File',
    ],
    'check' => [
        'label' => 'Mobile No / Tracking Id',
        'placeholder' => 'Mobile No / Tracking Id',
    ],
    'certificate_number' => [
        'label' => 'Certificate Number',
        'placeholder' => 'Certificate Number',
    ],
    'certificate_no' => [
        'label' => 'Unique Certificate No',
        'placeholder' => 'Unique Certificate No',
    ],
    'verify' => [
        'verifiers_name' => 'Verifiers Name',
        'verifiers_address' => 'Verifiers Address',
        'placeholder' => 'Insert certificate no you want to verify',
    ],
    'contact_name' => [
        'label' => 'Name',
        'placeholder' => 'Name',
    ],
    'subject' => [
        'label' => 'Subject',
        'placeholder' => 'Subject',
    ],
    'message' => [
        'label' => 'Message',
        'placeholder' => 'Message',
    ],
    'lostcertificate_checkby' => [
        'label' => 'For Lost Certificate Please Select',
        'certificate_no' => 'Certificate No.',
        'birth_reg_no' => 'Birth Reg No.',
    ],
    'no_such_certificate_found'      => 'No Such Certificate Found!',
    'apply_certificate_default'      => 'New Certificate Created.',
    'apply_certificate_message'      => 'New Certificate Created Successfully.',
    'lost_certificate_nothing_found' => 'No Such Certificate Found! Create A New Certificate First. <a href="{{route("apply")}}">click here</a>',
    'lost_certificate_default'       => 'Requested For Lost Certificate.',
    'lost_certificate_message'       => 'Requested For Lost Certificate Successfully.',
    'check_certificate_nothing_found' => 'No Such Certificate Found! Create A New Certificate First. <a href="{{route("apply")}}">click here</a>',
    'contact_default'                => 'Message sent.',
    'contact_message'                => 'Message Sent Successfully.',
];
?>
