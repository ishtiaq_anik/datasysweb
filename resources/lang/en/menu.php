<?php

return [

    'home' => "Home",
    'certificate' => 'Certificate',
    'certificates' => [
        'new' => 'New Certificate',
        'lost' => 'Lost Certificate',
        'check' => 'Check Certificate Status',
        'location' => 'Delivery Point'
    ],
    'verification' => 'Verification',
    'faq' => "Faq",
    "contact" => "Contact",
    "language" => [
        'id' => "bn",
        'name' => "বাংলা",
        'src' => "/images/bd.png",
    ],
];
