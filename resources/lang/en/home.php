<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'slider' =>[
        'title1' => 'Apply Online For <br> Permanent Residence Certificates ',
        'button-title1'=>'Apply',
        'title2' => 'Apply Online For <br> Permanent Residence Certificates ',
        'button-title2'=>'Apply'

    ],

    'apply' =>[
        'title'=>'Apply for Permanent Residentship',
        'subtitle'=>'Apply for Permanent Residentship',
        'button-title'=>'Apply'
    ],

    'dc' =>[
        'title' => "Deputy Comissioner's Message",
        'notice' => 'Notices & Events',
        'office' =>"Deputy Comissioner",//'s Office
        'address' => 'Rangamati Hill District, Chittagong Division'
    ],
    'service' =>[
        'header' => [
            'title'=>'Our Services',
        ],
        'one' => [
            'title'=>'Apply for new certificate',
            'button'=>'Apply Now',
            'subtitle1'=>'1 copy passport size photo',
            'subtitle2'=>'Birth cirtificate online copy',
            'subtitle3'=>'Mayor or chairmain certificate online copy',
            'subtitle4'=>'Natioanal Id online copy (If any)',

        ],
        'two' => [
            'title'=>'Apply for lost certificate',
            'button'=>'Apply Now',
            'subtitle1'=>'GD online copy',
            'subtitle2'=>'Birth registration number',
            'subtitle3'=>'Lost certificate number',
            'subtitle4'=>'Natioanal Id number (If any)',
        ],
        'three' => [
            'title'=>'Check your certificate status',
            'button'=>'Check Now',
            'subtitle'=>'A tracking id that sent you via sms.',
        ],
        'four' => [
            'title'=>'Verify any ceritificate to check validity',
            'button'=>'Verify Now',
            'subtitle'=>'Certificate number to verify',
        ],

    ],
    'certificate_image' =>[
        // 'img' => '<img alt="" class="home_certificate" src="/assets/base/img/content/certificates/certificate_en.jpg">',
         'img' => '<img alt="" height="325px" width="100%" src="/assets/base/img/content/certificates/certificate_en.jpg">',
    ],

    'apply' =>[
        'apply_now' => 'Apply Now',
        'title'     => 'IT\'S EASY',
        'message'   => 'APPLY FOR YOUR CERTIFICATE IF YOU DON\'T HAVE ONE.',
    ],

    'about_service' =>[
        'title'     => 'About The Process',
        'sub_title' => 'About The Process',
        'explore'   => 'Explore',
        'contact_us'=> 'Contact Us',
        'one' => [
            'title'     => 'APPLY FOR NEW CERTIFICATE',
            'content'   => 'You have to apply for a new certificate by filling the form.',
        ],
        'two' => [
            'title'     => 'CHECK PROGRESS STATUS',
            'content'   => 'After applying for a new certificate you can check the progress.',
        ],
        'three' => [
            'title'     => 'CERTIFICATE COLLECTION',
            'content'   => 'You can collect your certificate from UDC/DC office',
        ],
    ],
    'footer' => [
        'useful_link' => [
            'title' => 'Useful Links',
            'ministry_of_public_administration' => 'MINISTRY OF PUBLIC ADMINISTRATION',
            'national_web_portal' => 'NATIONAL WEB PORTAL',
            'prime_minister_office' => 'PRIME MINISTER\'S OFFICE',
            'bangladesh_tourism_board' => 'BANGLADESH TOURISM BOARD',
            'hilltracts_development_board' => 'HILLTRACTS DEVELOPMENT BOARD',
            'hilltracts_regional_council' => 'CHITTAGONG HILLTRACTS REGIONAL COUNCIL',
        ],
        'find_us' => [
            'title' => 'Find Us',
        ],
    ],
];
