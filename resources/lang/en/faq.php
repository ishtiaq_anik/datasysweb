<?php

return [

    'header' => [
        'all' => 'All',
        'new' => 'New Certificate',
        'lost' => 'Lost Certificate',
        'verify' => 'Certificate Verification'
    ],

    'new_certificate' =>[

        'qone' => [
            'ques'=>'Do I need N.I.D card for applying new application?',
            'ans'=>'Yes you need that or you can apply with your birth cerificate.',
        ],
        'qtwo' => [
            'ques'=>'Do I need N.I.D card for applying new application?',
            'ans'=>'Yes you need that or you can apply with your birth cerificate.',
        ],
        'qthree' => [
            'ques'=>'Do I need N.I.D card for applying new application?',
            'ans'=>'Yes you need that or you can apply with your birth cerificate.',
        ],
        'qfour' => [
            'ques'=>'Do I need N.I.D card for applying new application?',
            'ans'=>'Yes you need that or you can apply with your birth cerificate.',

        ],

    ],

    'lost_certificate' =>[

        'qone' => [
            'ques'=>'Do I need N.I.D card for applying lost application?',
            'ans'=>"No, you don't need that. You just need your previous certificate's number",
        ],
        'qtwo' => [
            'ques'=>'Do I need N.I.D card for applying lost application?',
            'ans'=>"No, you don't need that. You just need your previous certificate's number",
        ],
        'qthree' => [
            'ques'=>'Do I need N.I.D card for applying lost application?',
            'ans'=>"No, you don't need that. You just need your previous certificate's number",
        ],
        'qfour' => [
            'ques'=>'Do I need N.I.D card for applying lost application?',
            'ans'=>"No, you don't need that. You just need your previous certificate's number",

        ],

    ],

    'certificate_verification' =>[

        'qone' => [
            'ques'=>'Do I need N.I.D card for Certificate Verification ?',
            'ans'=>"No, you don't need that. You just need person's certificate's number",
        ],
        'qtwo' => [
            'ques'=>'Do I need N.I.D card for Certificate Verification ?',
            'ans'=>"No, you don't need that. You just need your person's certificate's number",
        ],
        'qthree' => [
            'ques'=>'Do I need N.I.D card for Certificate Verification ?',
            'ans'=>"No, you don't need that. You just need your person's certificate's number",
        ],
        'qfour' => [
            'ques'=>'Do I need N.I.D card for Certificate Verification ?',
            'ans'=>"No, you don't need that. You just need your person's certificate's number",

        ],

    ],

];
