<?php

return [
    'application' => "Permanent Residence Certificate",
    'home' => "Home",

    'faq' => [
        'title' => 'FAQ',
        'subtitle' => 'All questions in one page',
    ],
    'contact' => [
        'title' => "Contact",
        'subtitle' => "Contact",
    ],
    'apply' => [
        'title' => "New Certificate",
        'subtitle' => "Apply For New Certificate",
    ],
    'success' => [
        'title' => "Participation request successful",
        'subtitle' => "Participation request successful",
    ],
];

?>
