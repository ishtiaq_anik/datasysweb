
require('./jquery/jquery');
require('./bootstrap/transition');
require('./bootstrap/carousel');
require('./bootstrap/scrollspy');
require('./bootstrap/collapse');
// require('./bootstrap/alert');
// require('./bootstrap/button');
// require('./bootstrap/dropdown');
// require('./bootstrap/modal');
// require('./bootstrap/tab');
// require('./bootstrap/affix');
// require('./bootstrap/tooltip');
// require('./bootstrap/popover');

// require('./jquery.easing');
require('./jquery.parallax');
// require('./smooth-scroll');
// require('./owl.carousel');
// require('./jquery.vticker');
// require('./jquery.cubeportfolio.js');
// require('./portfolio.js');
//
require('./layout.js');
require('./bs-carousel.js');
require('./form-submit');
