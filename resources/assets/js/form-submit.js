$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
$(".select-toggle").on("click", function(){ $("input[type=checkbox]").prop('checked', $(this).prop('checked')); });

function appAlert(message, status) {
    var container = ".portlet-body";
    var place = "prepend";
    status = (status !== 'undefined') ? status : 'danger';
    App.alert({
        type: status,
        icon: status,
        message: message,
        container: container,
        place: place
    });
}
function validation_check(data) {
    if (data.validation) {
        appAlert("Validation error found", "danger");
        var errors = data.errors;
        for (var field in errors) {
            var group = $(".has-error-" + field);
            if (group.length) {
                if (group.children(".help-block").length) {
                    group.children(".help-block").remove();
                }
                if (!group.hasClass("has-error")) {
                    group.addClass("has-error");
                }
                group.append("<span class='help-block'><strong>" + errors[field][0] + "</strong></span>");
            }
        }
    }
}

$("#ajax-form").on("submit", function (e) {
    e.preventDefault();
    var form_data = new FormData(this);
    var action = $('input[name="action"]').val();
    var method = $('input[name="method"]').val();
    var response_type = "form_response";
    NProgress.start();
    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = parseInt(evt.loaded / evt.total);
                    NProgress.set(percentComplete);
                }
            }, false);
            return xhr;
        },
        url: action,
        type: method,
        data: form_data,
        cache: false,
        contentType: false,
        processData: false
    }).done(function (response) {
        if (response.success) {
            $(".has-error").children(".help-block").remove();
            $(".has-error").removeClass("has-error");
            appAlert(response.message, 'success');
            if (response.data !== undefined) {
                if (response.data.redirect_to !== undefined) {
                    window.location = response.data.redirect_to;
                }
            }
        } else if(response.validation) {
            validation_check(response);
        }else {
            appAlert(response.message, 'danger');
        }
    }).fail(function (response) {
        appAlert(response.message, "danger");
    }).always(function () {
        NProgress.done();
    });
});
$("#select_district").on("change", function (e) {
    e.preventDefault();
    var district = $(this);
    var target = $(this).data('target');
    $.ajax({
        url : district.data('action'),
        method : 'post',
        data : {
            'district_id' : district.val()
        }
    }).done(function (response) {
        var option_text = $(target + " option:first-child").html();
        html = '<option value="0">' + option_text + '</option>';
        $.each(response.data, function( index, value ) {
            html += '<option value="' +value.id+ '">' +value.name+ '</option>'
        });
        $(target).html(html);
    }).fail(function (response) {
        appAlert(response.message, "danger");
    });
});
