<?php

namespace App\Http\Components;

use App\Http\Components\Curl;

class InfobipAPI {

    private $username = "akijcd";
    private $password = "W1wp48jm";

    public function getAuthorization() {
        $username = $this->username;
        $password = $this->password;
        return base64_encode("$username:$password");
    }

    public function sendSMSMessage($message_json) {

        $url = "http://api.infobip.com/sms/1/text/multi";
        $method = "POST";
        $header = [
            "accept: application/json",
            "authorization: Basic " . $this->getAuthorization(),
            "content-type: application/json"
        ];
        $post_fields = $message_json;

        return Curl::call($url, $method, $header, $post_fields);
    }

    public function getDeliveryReports() {

        $url = "http://api.infobip.com/sms/1/reports";
        $method = "GET";
        $header = [
            "accept: application/json",
            "authorization: Basic " . $this->getAuthorization()
        ];
        $post_fields = "";

        return Curl::call($url, $method, $header, $post_fields);
    }

    public function getSMSLogs($from = "", $to = "", $limit = 1) {

        $url = "http://api.infobip.com/sms/1/logs?from=$from&limit=$limit";
        $url .= (!empty($to)) ? "&to=$to" : "";

        $method = "GET";
        $header = [
            "accept: application/json",
            "authorization: Basic " . $this->getAuthorization()
        ];
        $post_fields = "";

        return Curl::call($url, $method, $header, $post_fields);
    }

}
