<?php

namespace App\Http\Components;
use Image;
use File;
use Illuminate\Support\Facades\Storage;


trait FileHandle {

    /*
     * save file in Storage
     */
    public function saveInStorage($file, $folder){
        $fileName = time().'.'.$file->getClientOriginalExtension();
        $fl = Image::make($file->getRealPath());
        $fl->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio();
        });
        $fl->stream();

        Storage::disk('local')->put('public/'.$folder.'//thumbnail//'.$fileName, $fl, 'public');
        $f2 = Image::make($file->getRealPath());
        $f2->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        });
        $f2->stream();
        Storage::disk('local')->put('public/'.$folder.'//original//'.$fileName, $f2, 'public');

        return $fileName;
    }
    
    public function mobileNumberEnglishConversion($number){
        $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
        $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

        return str_replace($bn, $en, $number);
    }

}
