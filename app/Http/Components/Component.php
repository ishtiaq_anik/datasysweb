<?php

namespace App\Http\Components;
use App\Project;

trait Component {
    public $validation = 1;
    public $success = false;
    public $message = "Request processing failed";
    public $data = [];
    public $errors = [];
    public $html = [];

    public function validationError() {
        $data = [
            'validation' => $this->validation,
            'errors' => $this->errors,
        ];
        return $data;
    }

    public function output() {
        $data = [
            'success' => $this->success,
            'data' => $this->data,
        ];

        if (count($this->message)) {
            $data['message'] = $this->message;
        }
        if (count($this->html)) {
            $data['html'] = $this->html;
        }

        return $data;
    }

    public function success() {
        $this->success = true;
    }

    public function setAlert($message) {
        session(['alert' => $message]);
    }

    public function setAlertPermission() {
        session(['alert' => "Currently You don't have permission to visit that page"]);
    }

    public function getAlert() {
        return (session()->has('alert') ? session()->pull('alert') : null);
    }

    public function setAlertCSSClass($type = "") {
        session(['alertType' => (($type == "success") ? "alert-success" : "alert-danger")]);
    }

    public function getAlertCSSClass() {
        return (session()->has('alertType') ? session()->pull('alertType') : "alert-danger");
    }

    public function getDayFromSlot($slot){
      $data = explode("_", $slot);
      $day = ["Friday", "Saturday"];
      $date = "";
      switch ($data[0]) {
        case '17':
          $date .= $day[0];
          break;
        case '18':
          $date .= $day[1];
          break;
        default:
          break;
      }
      return $date;
    }

    public function getTimeFromSlot($competition_id, $slot){
      $data = explode("_", $slot);
      $slot = (int)$data[1];
      $time = "";
      if( $competition_id = 2 ){
        $slot-=1;
      }
      switch($slot){
        case 0:
          $time = "11AM-12PM";
          break;
        case 1:
          $time = "02:30PM – 03:30PM";
          break;
        case 2:
          $time = "03:30PM-04:30PM";
          break;
        default:
          $time = "--";
          break;
      }
      return $time;
    }

}
