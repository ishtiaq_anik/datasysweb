<?php

namespace App\Http\Datatables;

use App\Participant;
use App\Components;
use Auth;
use Illuminate\Http\Request;

class ParticipantDatatable extends Datatable {

    private $nav = 'participants';

    public function table(Request $request) {
        //dd('dd inside');
        $this->deleteTableItem($request);
        $results = new Participant;//::orderBy('created_at', 'desc');

        $name = isset($request->name) ? $request->name : null;
        $father = isset($request->father) ? $request->father : null;
        $mother = isset($request->mother) ? $request->mother : null;
        $code = isset($request->serial) ? $request->serial : null;
        $dob = isset($request->dob) ? $request->dob : null;
        $age = isset($request->age) ? $request->age : null;
        $email = isset($request->email) ? $request->email : null;
        $mobile = isset($request->mobile) ? $request->mobile : null;
        $school = isset($request->school) ? $request->school : null;
        $class = isset($request->class) ? $request->class : null;

        $updated_from = isset($request->updated_from) ? $request->updated_from : null;
        $updated_to = isset($request->updated_to) ? $request->updated_to : null;

        if ($name) {
            $results = $results->where('name', 'like', '%' . $name . '%');
        }
        if ($father) {
            $results = $results->where('father', 'like', '%' . $father . '%');
        }
        if ($mother) {
            $results = $results->where('mother', 'like', '%' . $mother . '%');
        }
        if ($age) {
            $results = $results->where('age', '=', $age);
        }
        if ($dob) {
            $results = $results->where('dob', '=', $dob);
        }
        if ($code) {
            $results = $results->where('code', '=', $code);
        }
        if ($mobile) {
            $results = $results->where('mobile', 'like', '%' . $mobile . '%');
        }
        if ($email) {
            $results = $results->where('email', 'like', '%' . $email . '%');
        }
        if ($school) {
            $results = $results->where('school', 'like', '%' . $school . '%');
        }
        if ($class) {
            $results = $results->where('class', 'like', '%' . $class . '%');
        }
        if ($updated_from) {
            $results = $results->where('updated_at', '>=', $this->date_filter($updated_from));
        }
        if ($updated_to) {
            $results = $results->where('updated_at', '<=', $this->date_filter($updated_to, true));
        }

        $tableColumns = [
            "",
            "",
            "name",
            "father",
            "mother",
            "dob",
            "age",
            "",
            "",
            "email",
            "mobile",
            "updated_at",
        ];

        $results = $results->get();
        //$results = $results->select('mobile')->distinct()->get();
        //dd($results);


        $iTotalRecords = $results->count();
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $data = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $result = $results[$i];
            $delete = "<a class='btn btn-sm btn-outline red table-row-delete' title='Remove' href='javascript:;' data-id='" . $result->id . "'> <i class='icon-trash'></i> Remove</a>";
            $data[] = [
                $i+1,
                isset($result->code) ? $result->code : "",
                isset($result->name) ? $result->name : "",
                isset($result->father) ? $result->father : "",
                isset($result->mother) ? $result->mother : "",
                isset($result->dob) ? $result->dob : "",
                isset($result->age) ? $result->age : "",
                isset($result->school) ? $result->school : "",
                isset($result->class) ? $result->class : "",
                isset($result->email) ? $result->email : "",
                isset($result->mobile) ? $result->mobile: "",
                // $result->show ? '<span class="label bg-green-jungle"> Showing </span>' : '<span class="label label-danger"> Hidden </span>', participant_detail
                isset($result->updated_at) ? date_format($result->updated_at, 'd/m/Y') : "",
                // (isset($delete) ? $delete : ""),
            ];
        }
        $this->data = $data;
        $this->draw = $sEcho;
        $this->total = $iTotalRecords;
        $this->filtered = $iTotalRecords;
        return $this->outputDatatable();
    }

    public function deleteTableItem($request) {
        if (isset($request->actionType) && $request->actionType == "delete_action") {
            $company = Participant::find($request->record_id);
            if ($company) {
                $company->delete();
                $this->status = "OK";
                $this->message = "Participant deleted successfully";
            } else {
                $this->message = "Participant delete failed";
            }
        }
    }

}
