<?php

namespace App\Http\Datatables;

use App\Certificate;
use App\Components;
use Auth;
use Illuminate\Http\Request;

class CertificateDatatable extends Datatable {

    private $nav = 'certificates';

    public function table(Request $request) {

        $this->deleteTableItem($request);
        $results = new Certificate;//::orderBy('created_at', 'desc');
        $lang = isset($request->lang) ? $request->lang : null;
        $type = isset($request->type) ? $request->type : null;
    //    $name = isset($request->name) ? $request->name : null;
        $name = isset($request->name) ? $request->name : null;
        $name_bangla = isset($request->name_bangla) ? $request->name_bangla: null;
        $number = isset($request->number) ? $request->number : null;
        $tracking_id = isset($request->tracking_id) ? $request->tracking_id : null;
        $status = isset($request->status) ? $request->status : null;
        $mobile = isset($request->mobile) ? $request->mobile : null;
        $district = isset($request->district) ? $request->district : null;
        $updated_from = isset($request->updated_from) ? $request->updated_from : null;
        $updated_to = isset($request->updated_to) ? $request->updated_to : null;

        if ($lang) {
            $results = $results->where('lang', 'like', '%' . $lang . '%');
        }
        if ($type) {
            $results = $results->where('type', 'like', '%' . $type . '%');
        }
        if ($name) {
            $results = $results->where('name', 'like', '%' . $name . '%');
        }
        if ($name_bangla) {
            $results = $results->where('name_bangla', 'like', '%' . $name_bangla . '%');
        }
        if ($number) {
            $results = $results->where('number', 'like', '%' . $number . '%');
        }
        if ($tracking_id) {
            $results = $results->where('tracking_id', 'like', '%' . $tracking_id . '%');
        }
        if ($mobile) {
            $results = $results->where('mobile', 'like', '%' . $mobile . '%');
        }
        if ($status) {
            $permission_status = explode(" ",$status);
            $status="";
            for($i=0;$i<count($permission_status);$i++){
                $status.=$permission_status[$i];
                if(($i+1)<count($permission_status)){
                    $status.="-";
                }
            }
            $results = $results->where('status', 'like', '%' . $status. '%');
        }

        if ($updated_from) {
            $results = $results->where('updated_at', '>=', $this->date_filter($updated_from));
        }
        if ($updated_to) {
            $results = $results->where('updated_at', '<=', $this->date_filter($updated_to, true));
        }
        $tableColumns = [
            "",
            "lang",
            "type",
            "name",
            "name_bangla",
            "number",
            "tracking_id",
            "mobile",
            "",
            "status",
            "updated_at",
            ""
        ];
        $sortColumn = $request->order[0]['column'];
        $sortDir = $request->order[0]['dir'];
        $sort_field = $tableColumns[$sortColumn];

        switch ($sort_field) {
            case "lang":
            $results = $results->orderBy('lang', $sortDir);
            break;
            case "type":
            $results = $results->orderBy('type', $sortDir);
            break;
            case "name":
            $results = $results->orderBy('name', $sortDir);
            break;
            case "name_bangla":
            $results = $results->orderBy('name_bangla', $sortDir);
            break;
            case "status":
            $results = $results->orderBy('status', $sortDir);
            break;
            case "mobile":
            $results = $results->orderBy('mobile', $sortDir);
            break;
            case "tracking_id":
            $results = $results->orderBy('tracking_id', $sortDir);
            break;
            case "updated_at":
            $results = $results->orderBy('updated_at', $sortDir);
            break;
        }
        // For Uno

        $group_id = Auth::user()->group_id;
       
        if($group_id==10){
            $results = $results->whereStatus("sent-to-uno-for-verification");
            $office = Auth::user()->office;
            if($office) {
                $subdistrict = $office->subdistrict;
            }
            $subdistrict_id = isset($subdistrict) ? $subdistrict->id : 0;
            if($subdistrict_id) {
                $results = $results;
            }
            if($subdistrict_id){
                $results = $results->where('subdistrict_id', '=', $subdistrict_id);
            }
        }

        // For UDC Enterpreneur


        if($group_id==12){
            $results = $results->whereStatus("sent-to-udc")->whereOfficeId(Auth::user()->office_id);

        }

        $results = $results->get();
        // dd($results[0]);
        //$group_id = Auth::user()->group_id;
        //$certificate = Certificate::whereId($request->certificate)->whereStatus("sent-to-uno-for-verification")->get();


        $iTotalRecords = $results->count();
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $data = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $result = $results[$i];

            if($this->checkPermission($this->nav, 'update')) {
                // $edit = "<a class='btn btn-sm btn-outline blue' title='Edit' href='" . route('certificate_edit', [$result->id]) . "'> <i class='icon-note'></i> Edit</a>";
            }
            if($this->checkPermission($this->nav, 'delete')) {
                $delete = "<a class='btn btn-sm btn-outline red table-row-delete' title='Remove' href='javascript:;' data-id='" . $result->id . "'> <i class='icon-trash'></i> Remove</a>";
            }


            // For Uno
            if($group_id==8){
                $detail = "<a class='btn btn-sm btn-outline blue' title='Detail' href='" . route('certificate_detail', [$result->id]) . "'> <i class='icon-note'></i> Detail</a>";
            }else{
                $detail = "<a class='btn btn-sm btn-outline blue' title='Detail' href='" . route('certificate_detail', [$result->id]) . "'> <i class='icon-note'></i> Detail</a>";
                if($result->status=="sent-to-udc" || $result->status=="ready-to-collect"){
                    $print = "<a class='btn btn-sm btn-outline blue btn-print' title='Print' href='" . route('certificate_print', [$result->id]) . "'> <i class='icon-note'></i> Print</a>";
                }else{
                    $print="";
                }
            }

            $status = "";
            if(isset($result->status)) {
                $status = $this->residenceStatuses()[$result->status];
                $status = "<label class='label ".$status['color']."'>" . $status['title'] . "</label>";
            }

            $data[] = [
                $i+1,
                isset($result->lang) ? $result->lang : "",
                isset($result->type) ? $result->type : "",
            //    isset($result->name) ? $result->name : "",
                empty($result->name) ? $result->name_bangla : $result->name,
            //    isset($result->name_bangla) ? $result->name_bangla : "",
                isset($result->number) ? $result->number : "",
                isset($result->tracking_id) ? $result->tracking_id : "",
                isset($result->mobile) ? $result->mobile : "",
                isset($result->district) ? $result->district->name : "",
                $status,
                // $result->show ? '<span class="label bg-green-jungle"> Showing </span>' : '<span class="label label-danger"> Hidden </span>', certificate_detail
                isset($result->updated_at) ? date_format($result->updated_at, 'd/m/Y') : "",
                (isset($edit) ? $edit : "") . (isset($delete) ? $delete : "") . (isset($detail) ? $detail : "") . (isset($print) ? $print : ""),
            ];
        }
        $this->data = $data;
        $this->draw = $sEcho;
        $this->total = $iTotalRecords;
        $this->filtered = $iTotalRecords;
        return $this->outputDatatable();
    }

    public function deleteTableItem($request) {
        if (isset($request->actionType) && $request->actionType == "delete_action") {
            $company = Certificate::find($request->record_id);
            if ($company) {
                $company->delete();
                $this->status = "OK";
                $this->message = "Certificate deleted successfully";
            } else {
                $this->message = "Certificate delete failed";
            }
        }
    }

}
