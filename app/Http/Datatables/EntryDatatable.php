<?php

namespace App\Http\Datatables;

use App\Entry;
use App\Components;
use Auth;
use Illuminate\Http\Request;

class EntryDatatable extends Datatable {

    private $nav = 'entries';

    public function table(Request $request) {
        //dd('dd inside');
        $this->deleteTableItem($request);
        $results = new Entry;//::orderBy('created_at', 'desc');

        // $id = isset($request->id) ? $request->id : null;
        // $updated_from = isset($request->updated_from) ? $request->updated_from : null;
        // $updated_to = isset($request->updated_to) ? $request->updated_to : null;

        $results = $results->get();

        $iTotalRecords = $results->count();
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);

        $data = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $result = $results[$i];
            $delete = "<a class='btn btn-sm btn-outline red table-row-delete' title='Remove' href='javascript:;' data-id='" . $result->id . "'> <i class='icon-trash'></i> Remove</a>";
            $data[] = [
                $i+1,
                isset($result->participant_id) ? $result->participant->father : "",
                isset($result->participant_id) ? $result->participant->mother : "",
                isset($result->participant_id) ? $result->participant->name : "",
                isset($result->participant_id) ? $result->participant->code : "",
                isset($result->participant_id) ? date("d M, Y", strtotime($result->participant->dob)) : "",
                isset($result->participant_id) ? $result->participant->age : "",
                isset($result->participant_id) ? $result->participant->school : "",
                isset($result->participant_id) ? $result->participant->class : "",
                isset($result->competition_id) ? $result->competition->name : "",
                isset($result->competition_id) ? $result->competition->name_bangla : "",
                isset($result->slot) ? $this->getDayFromSlot($result->slot) : "",
                isset($result->slot) ? $this->getTimeFromSlot($result->competition_id, $result->slot) : "",
                isset($result->participant_id) ? $result->participant->mobile : "",
                isset($result->participant_id) ? $result->participant->email : "",
                // $result->show ? '<span class="label bg-green-jungle"> Showing </span>' : '<span class="label label-danger"> Hidden </span>', participant_detail
                isset($result->updated_at) ? date_format($result->updated_at, 'd/m/Y') : "",
                // (isset($delete) ? $delete : ""),
            ];
        }
        $this->data = $data;
        $this->draw = $sEcho;
        $this->total = $iTotalRecords;
        $this->filtered = $iTotalRecords;
        return $this->outputDatatable();
    }

    public function deleteTableItem($request) {
        if (isset($request->actionType) && $request->actionType == "delete_action") {
            $company = Participant::find($request->record_id);
            if ($company) {
                $company->delete();
                $this->status = "OK";
                $this->message = "Participant deleted successfully";
            } else {
                $this->message = "Participant delete failed";
            }
        }
    }

}
