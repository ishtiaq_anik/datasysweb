<?php

namespace App\Http\Controllers;

use Validator;
use App\Certificate;
use App\District;
use App\Office;
use App\PostOffice;
use App\Subdistrict;
use App\Report;
use App\Union;
use App\User;
use Auth;
use App\Http\Datatables\CertificateDatatable;
use Illuminate\Http\Request;
use PDF;
use App\Http\Components\FileHandle;
use App\Http\Components\InfobipAPI;
use View;

class CertificateController extends Controller {

    private $nav = 'certificates';

    function __construct()
    {
    //    $this->middleware('menu');
    }

    public function getMessageJson($sender, $receiver, $message) {
        return "{ \"messages\":[ { \"from\":\"$sender\", \"to\":\"$receiver\", \"text\":\"$message\" } ] }";
    }

    public function index() {
        if($this->checkPermission($this->nav, 'show')) {
            $params = [
                'base_url' => route('certificates'),
                'dataload_url' => route('certificates_load'),
                'title' => "Certificate",
                'titles' => "Certificates",
                'icon' => $this->getIcons($this->nav),
                'icons' => $this->getIcons($this->nav, true),
                'create' => $this->checkPermission($this->nav, 'create'),
                'filter' => true,
                'columns' => [
                    [ "title" => "#", "width" => "5%", "filter" => ""],
                    [ "title" => "Language", "filter" => $this->filterText("lang")],
                    [ "title" => "Type", "filter" => $this->filterText("type")],
                    [ "title" => "Name", "filter" => $this->filterText("name")],
                //    [ "title" => "নাম", "filter" => $this->filterText("name_bangla")],
                    [ "title" => "Certificate", "filter" => $this->filterText("number")],
                    [ "title" => "Tracking Id", "filter" => $this->filterText("tracking_id")],
                    [ "title" => "Mobile", "filter" => $this->filterText("mobile")],
                    [ "title" => "District", "filter" => $this->filterText("district")],
                    [ "title" => "Status", "filter" => $this->filterText("status")],
                    [ "title" => "Updated Time", "filter" => $this->filterDateRange()],
                    [ "title" => "Action", "filter" => $this->filterAction()],
                ],
            ];
            $params['message'] = $this->getAlert();
            $params['messageType'] = $this->getAlertCSSClass();

            return view('table', $params)->withNav($this->nav);
        }else {
            $this->setAlertPermission();
            return redirect(route("home"));
        }
    }

    public function datatable(Request $request) {
        $ajax_table = new CertificateDatatable;
        return $ajax_table->table($request);
    }

    public function detail(Request $request) {
        $certificate = Certificate::find($request->certificate);
        $project_path = isset($certificate->project) ? $certificate->project->url : "";
        if ($certificate) {
            $params = [
                'certificate' => $certificate,
                'picture_thumbnail' => $this->storage_link($project_path, 'images/thumbnail', $certificate->picture),
                'picture_original' => $this->storage_link($project_path, 'images/original', $certificate->picture),
                'national_id_file_thumbnail' => $this->storage_link($project_path, 'files/nid_file/thumbnail', $certificate->national_id_file),
                'national_id_file_original' => $this->storage_link($project_path, 'files/nid_file/original', $certificate->national_id_file),
                'chairman_certificate_file_thumbnail' => $this->storage_link($project_path, 'files/cm_certificate/thumbnail', $certificate->chairman_certificate_file),
                'chairman_certificate_file_original' => $this->storage_link($project_path, 'files/cm_certificate/original', $certificate->chairman_certificate_file),
                'birth_certificate_file_thumbnail'=> $this->storage_link($project_path, 'files/birth_file/thumbnail', $certificate->birth_certificate_file),
                'birth_certificate_file_original'=> $this->storage_link($project_path, 'files/birth_file/original', $certificate->birth_certificate_file),
                'certificate_number' => isset($certificate->number) ? $certificate->number : "",
                'district' => isset($certificate->district) ? $certificate->district->name : "",
                'subdistrict' => isset($certificate->subdistrict) ? $certificate->subdistrict->name : "",
                'union' => isset($certificate->union) ? $certificate->union->name : "",
                'ward' => $certificate->ward_id . " Number Ward",
                'office' => isset($certificate->office) ? $certificate->office->name : "",
                'post_office' => isset($certificate->post_office) ? $certificate->post_office->name : "",
                'tracking_id' => $certificate->tracking_id,
                'status' => $this->getResidenceStatus($certificate->status),
            ];
        //   dd( $params);
            return view('certificates-detail', $params)->withNav($this->nav);
        }
        return redirect(route($this->nav));
    }

    public function edit(Request $request) {
        $certificate = Certificate::find($request->certificate);
        if ($certificate) {
            return view('certificates-create', compact('certificate'))->withNav($this->nav);
        }
        return redirect(route($this->nav));
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'certificate_description' => 'required|max:255',
            'certificate_description_bangla' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            return $this->validationError();
        }
        if ($request->certificate) {
            $certificate = Certificate::find($request->certificate);
        } else {
            $certificate = new Certificate;
        }
        $certificate->user_id = Auth::user()->id;
        $certificate->description = $request->input('certificate_description');
        $certificate->description_bangla = $request->input('certificate_description_bangla');
        $certificate->show = ($request->input('show_certificates') != null) ? true : false ;
        if ($certificate->save()) {
            $this->success();
            $this->message = ($request->certificate) ? "Certificate Updated" : "New Certificate Added";
            if (!$request->certificate) {
                $data['redirect_to'] = route('certificates');
                $this->setAlert($this->message);
                $this->setAlertCSSClass("success");
            }
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    public function statusUpdate(Request $request) {

        $certificate = Certificate::find((int)$request->id);
        $new_status = $request->status;
        $report = new Report;

        if($certificate && $this->checkPermission($this->nav, $new_status)) {
            $previous_status = $certificate->status;
            $certificate->status = $new_status;
            $report->user_id = Auth::user()->id;
            $report->previous_status = $previous_status;
            $report->changed_status = $new_status;
            $report->certificate_id = $certificate->id;
            $report->project_id = $certificate->project_id;
            if ($certificate->save() && $report->save()) {
                if($new_status == "sent-to-udc" || $new_status == "ready-to-collect"){
                    /**/
                    $certificate->number = $this->generateCertificateNumber($certificate->id);
                    $certificate->save();
                    $sender = $certificate->district->name;
                    $receiver = "88" . $certificate->mobile;

                    $date = date_add($certificate->updated_at, date_interval_create_from_date_string('3 days'));
                    $collection_date = $date->format('d M, Y');

                    $message = "Your certificate is ready to collect. You can collect your certificate from ".$certificate->office->name.' after '.$collection_date;

                    $message_json = $this->getMessageJson($sender, $receiver, $message);
                    $api_sms = new InfobipAPI;

                    if($send_message = true) {
                        $sent_response = $api_sms->sendSMSMessage($message_json);
                    }
                }
                /* Send sms to UNO for verification*/
                if($new_status == "sent-to-uno-for-verification"){
                    $user = $this->getAppropriateUNO($certificate->subdistrict_id, $certificate->office_id);
                    if($user){
                        $name;
                        if($certificate->lang=="bangla"){
                            $name=$certificate->name_bangla;
                        }else{
                            $name=$certificate->name;
                        }
                        $application_date = date('d m, y', strtotime(str_replace('/', '-', $certificate->created_at)));
                        $message = "Please verify the user named ".$name." who applied for certification on ".$application_date." and is certificate collecting location is ".$certificate->office->name.". Thank you!";
                        $this->sendNotification($user, $message, null, true, false, $certificate, null);
                    }else{
                        //dd("getAppropriateUNO() in CertificateController: error");
                    }
                }
                $this->success();
                $this->message = "";
                $data = $this->getResidenceStatus($certificate->status);
            }
        }
        $this->data = isset($data) ? $data : [];
        return $this->output();
    }

    /* Print PDF of a certificates */
    public function printCertificatePDF($id){
        $id = (int)$id;
        $certificate = Certificate::whereId($id)->whereStatus('sent-to-udc')->orWhere('status','ready-to-collect')->orWhere('status','ready-to-collect')->first();
        if($certificate){
            /* Get DC Signature */
            $dc = User::whereGroupId(5)->whereDistrictId($certificate->district_id)->first();
            $compared_by = Report::whereCertificateId((int)$certificate->id)->whereChangedStatus('verification-done-approved')->first();
            $prepared_by = Report::whereCertificateId((int)$certificate->id)->where('changed_status','sent-to-udc')->orWhere('changed_status','ready-to-collect')->first();

            if(!$dc || !$compared_by || !$prepared_by){
                return redirect()->back()->withErrors(['msg', 'Something is missing!']);
            }
            $compared_by = $compared_by->user;
            $prepared_by = $prepared_by->user;
            $dc_sign = $this->copyFileToPublic($dc->signature, "uploads", 'public/signatures', true);
            $compared_by_sign = $this->copyFileToPublic($compared_by->signature, "uploads", 'public/signatures', true);
            $prepared_by_sign = $this->copyFileToPublic($prepared_by->signature, "uploads", 'public/signatures', true);

            if($dc_sign == "" || $compared_by_sign == "" || $prepared_by_sign == ""){
                return redirect()->back()->withErrors(['msg', 'Something is missing!']);
            }

            $pdf_view = 'certificates_pdf';
            if($certificate->lang == "bangla"){
                $pdf_view = 'certificates_bangla_pdf';
            }

            $mpdf = new \Mpdf\Mpdf([
                'default_font_size' => 12,
                'default_font' => 'nikoshban',
                'mode' => 'utf-8',
                'format' => 'A4-L',
                //[1500, 1800],
                'margin' => 0
            ]);
            $view = View::make($pdf_view)->with(['certificate' => $certificate, 'dc_sign' => $dc_sign, 'compared_by_sign' => $compared_by_sign, 'prepared_by_sign' => $prepared_by_sign]);
            $mpdf->WriteHTML($view);
            $mpdf->Output($certificate->number.'.pdf', 'D');

            // $pdf = PDF::loadView($pdf_view, ['certificate' => $certificate, 'dc_sign' => $dc_sign, 'compared_by_sign' => $compared_by_sign, 'prepared_by_sign' => $prepared_by_sign])->setPaper('a4', 'landscape')->setWarnings(false);

            //$pdf->save('certificate_'.$certificate->id.'.pdf');
            $this->deleteFileFromPublic($dc->signature, "uploads");
            $this->deleteFileFromPublic($compared_by->signature, "uploads");
            $this->deleteFileFromPublic($prepared_by->signature, "uploads");
            $certificate->status = 'certificate-printed';
            if($certificate->save()){
                return $pdf->download('certificate_'.$certificate->number.'.pdf');
            }
        }
        return redirect('home');
    }

    /*
     * Show PDF in the browser
     * https://github.com/barryvdh/laravel-dompdf/issues/57
     */
    public function showCertificatePdfLayout($id){
        $certificate = Certificate::find($id);
        if($certificate){
            /* Get DC Signature */
            $dc = User::whereGroupId(5)->whereDistrictId($certificate->district_id)->first();
            $compared_by = Report::whereCertificateId($certificate->id)->whereChangedStatus('verification-done-approved')->first()->user;
            $prepared_by = Report::whereCertificateId($certificate->id)->whereChangedStatus('sent-to-udc')->first()->user;
            if(!$dc || !$compared_by || !$prepared_by){
                return;
            }
            $dc_sign = $this->copyFileToPublic($dc->signature, "uploads", 'public/signatures', true);
            $compared_by_sign = $this->copyFileToPublic($compared_by->signature, "uploads", 'public/signatures', true);
            $prepared_by_sign = $this->copyFileToPublic($prepared_by->signature, "uploads", 'public/signatures', true);

            if($dc_sign == "" || $compared_by_sign == "" || $prepared_by_sign == ""){
                return;
            }

            $pdf_view = 'certificates_pdf';
            if($certificate->lang == "bangla"){
                $pdf_view = 'certificates_bangla_pdf';
            }
            $pdf = PDF::loadView($pdf_view, ['certificate' => $certificate, 'dc_sign' => $dc_sign, 'compared_by_sign' => $compared_by_sign, 'prepared_by_sign' => $prepared_by_sign])->setPaper('a4', 'landscape')->setWarnings(false);
            //$pdf->save('certificate_'.$certificate->id.'.pdf');
            $this->deleteFileFromPublic($dc->signature, "uploads");
            $this->deleteFileFromPublic($compared_by->signature, "uploads");
            $this->deleteFileFromPublic($prepared_by->signature, "uploads");
            return view($pdf_view, compact('certificate', 'dc_sign', 'compared_by_sign', 'prepared_by_sign'));
        }
        //return view('certificates_pdf',compact('certificate'));
    }


    public function generateCertificateNumber($id){
        $predefined_number = "AL.1010.2.120";
        $generated_number = "";
        $count = strlen("".$id);
        //The max length which can be added after $predefined_number
        $max_counter = 5;
        $max="";
        for($i=0;$i<$max_counter;$i++){
            $max.='9';
        }
        if((int)$id < (int)$max){
            if($count <= $max_counter){
                for($i=0;$i<($max_counter-$count);$i++){
                    $generated_number = $generated_number.'0';
                }
                $generated_number.=$id;
            }else{
                $generated_number = substr($id,0,$max_counter);
            }
            $certificate_number = $predefined_number.'.'.$generated_number;
            return $certificate_number;
        }
        return $certificate_number;
    }

}
