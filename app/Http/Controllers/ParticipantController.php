<?php

namespace App\Http\Controllers;

use Validator;
use App\Competition;
use App\Entry;
use App\Participant;
use Illuminate\Http\Request;
use App\Http\Components\InfobipAPI;
use App\Http\Components\FileHandle;
use Mail;
use Illuminate\Support\Facades\View;
use App\Mail\EmailWithCode;
use App\Http\Datatables\ParticipantDatatable;

class ParticipantController extends Controller
{
    public $global;
    function _construct() { $this->global = 1;}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = [
            'base_url' => "",
            'dataload_url' => route('participants_load'),
            'title' => "Participant",
            'titles' => "Participants",
            'icon' => "",//$this->getIcons($this->nav),
            'icons' => "",//$this->getIcons($this->nav, true),
            'create' => "",//$this->checkPermission($this->nav, 'create'),
            'filter' => true,
            'columns' => [
                [ "title" => "#", "width" => "5%", "filter" => ""],
                [ "title" => "Serial No.", "filter" => ""],
                //[ "title" => "Name", "filter" => $this->filterText("name")],
                [ "title" => "Name", "filter" => ""],
                [ "title" => "Father", "filter" => ""],
                [ "title" => "Mother", "filter" => ""],
                [ "title" => "Date of Birth", "filter" => ""],
                [ "title" => "Age", "filter" => ""],
                [ "title" => "School", "filter" => ""],
                [ "title" => "Class", "filter" => ""],
                [ "title" => "Email", "filter" => ""],
                [ "title" => "Mobile", "filter" => ""],
                [ "title" => "Updated Time", "filter" => ""],
                //[ "title" => "Updated Time", "filter" => $this->filterDateRange()],
                // [ "title" => "Action", "filter" => $this->filterText("")],
            ],
        ];
        $params['message'] = $this->getAlert();
        $params['messageType'] = $this->getAlertCSSClass();
        return view('table', $params);
    }

    public function datatable(Request $request) {
        $ajax_table = new ParticipantDatatable;
        return $ajax_table->table($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function show(Participant $participant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function edit(Participant $participant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participant $participant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participant $participant)
    {
        //
    }

    public function getMessageJson($sender, $receiver, $message) {
        return "{ \"messages\":[ { \"from\":\"$sender\", \"to\":\"$receiver\", \"text\":\"$message\" } ] }";
    }

    public function applyForm()
    {
        return view('apply');
    }

    public function applyStore(Request $request)
    {

  //     $participant= Participant::all();
  //
  // dd($participant);

        $competition_1 = $request->input('art');
        $competition_2 = $request->input('craft');
        $competition_3 = $request->input('writing');
        //dd(sizeof($competition_1),$competition_1, $competition_2, $competition_3);

        $name = $request->input('name');
        $age =  $request->input('age');
        $school = $request->input('school');
        $class = $request->input('class');
        $dob = $request->input('birth_date');
        //dd($name, $age, $school, $class, $dob);

        //Total submitted childrens
        $count = sizeof($request->input('name'));



        $validator_data = [
            'name.*'        => 'required|max:255',
            'age.*'         => 'required',
            'birth_date.*'  => 'required',
            'father'        => 'required|max:255',
            'mother'        => 'required|max:255',
            'school'        => 'max:255',
            'class'         => 'max:30',
            'birth_date'    => 'required',//|date|date_format:"d/m/Y"
            'email'         => 'max:255',
            'mobile'        => 'required|size:11',
        ];

        if(sizeof($competition_1)==0 and sizeof($competition_2)==0 and sizeof($competition_3)==0){
            $validator_data['art'] = 'required';
            $validator_data['craft'] = 'required';
            $validator_data['writing'] = 'required';
        }

        $validator = Validator::make($request->all(), $validator_data);
        if ($validator->fails()) {
            $this->errors = $validator->messages();
            //dd($this->errors);
            return $this->validationError();
        }
        //dd($request->all());

        //Total approved children
        $child_count = 0;
        $message = "";



        /* Code generate */
        $code = $this->generateCode($request->input('mobile'));

        for( $i = 1; $i <= $count; $i++ ){
            $participant = Participant::whereName($name[$i])->whereFather($request->input('father'))
            ->whereMother($request->input('mother'))->whereAge((int)$age[$i])->first();
            //->whereEmail($request->input('email'))->whereMobile($request->input('mobile'))
            if(!$participant){
                $participant = new Participant;
                $participant->name = $name[$i];
                $participant->father = $request->input('father');
                $participant->mother = $request->input('mother');
                $participant->age = $age[$i];
                $participant->school = isset($school[$i]) ? $school[$i] : NULL;
                $participant->class = isset($class[$i]) ? $class[$i] : NULL;
                $participant->dob = date('Y-m-d', strtotime(str_replace('/', '-', $dob[$i])));
                $participant->email = $request->input('email');
                $participant->mobile = $request->input('mobile');
                $participant->code = $code;
                $participant->status = 'active';
                $code_set = false;

                if ( $participant->save()) {
                    if($code_set==false){
                        $code = 1000 + $participant->distinct('code')->count('code');
                        $code_set=true;
                    }
                    $participant->code = $code;
                    $participant->save();
                    if( sizeof($competition_1)>0 ){
                        for($j=0; $j<2; $j++){
                          if(isset($competition_1[$i][$j])){
                            $entry = new Entry;
                            $entry->participant_id = $participant->id;
                            $entry->competition_id = 1;
                            $entry->slot = $competition_1[$i][$j];
                            $entry->status = 'active';
                            $entry->save();
                          }
                        }
                    }
                    if( sizeof($competition_2)>0 ){
                        for($j=0; $j<2; $j++){
                          if(isset($competition_2[$i][$j])){
                            $entry = new Entry;
                            $entry->participant_id = $participant->id;
                            $entry->competition_id = 2;
                            $entry->slot = $competition_2[$i][$j];
                            $entry->status = 'active';
                            $entry->save();
                          }
                        }
                    }
                    if( sizeof($competition_3)>0 ){
                        for($j=0; $j<2; $j++){
                          if(isset($competition_3[$i][$j])){
                            $entry = new Entry;
                            $entry->participant_id = $participant->id;
                            $entry->competition_id = 3;
                            $entry->slot = $competition_3[$i][$j];
                            $entry->status = 'active';
                            $entry->save();
                          }
                        }
                    }
                    $child_count++;
                }
            }
        }

        if($child_count>0){
            $sender = "datasys";
            $receiver = "88" . $participant->mobile;
            $message .= "Thanks for Registering in Farm Fresh Childrens Day 17. Registration Serial ".$code.". \\nCome to Kalabagan Central Field on 8 December.";

            $message_json = $this->getMessageJson($sender, $receiver, $message);
            $api_sms = new InfobipAPI;

            if($send_message = true) {
                $sent_response = $api_sms->sendSMSMessage($message_json);
            }

            //$to_email = $participant->email;
            //if($to_email){
                //Mail::to($to_email)->send(new EmailWithCode($participant->id, $message));
            //}

            $this->success();
            $default = trans('form.apply_participant_default');
            $this->message = trans('form.apply_participant_message');
            $data['redirect_to'] = route('success');
            $this->setAlert($this->message);
            $this->setAlertCSSClass("success");
        }

        $this->data = isset($data) ? $data : [];
        return $this->output();
    }


    public function displaySuccess(Request $request){
        $message = $this->getAlert();
        if(!empty($message)){
            return view('success',compact('message'));
        }
        return redirect('home');
    }

    public function generateCode($phone){
        $length = 5;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $randomString .= substr($phone, 8, 10);
        return $randomString;
    }
    public function generateSerialCode($phone){

        $participant= Participant::find($request->participant);
        return $participant;
        dd($participant);
    }

    public function addMoreTemplate($global){
        if (View::exists('add_more_child_template')) {
            //$template = View::make('add_more_child_template', $global);
            return view('add_more_child_template', compact('global'));
        }
    }

}
