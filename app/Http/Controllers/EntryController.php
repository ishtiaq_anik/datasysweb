<?php

namespace App\Http\Controllers;

use App\Entry;
use Illuminate\Http\Request;
use App\Http\Datatables\EntryDatatable;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $params = [
          'base_url' => "",
          'dataload_url' => route('entries_load'),
          'title' => "Entry",
          'titles' => "Entries",
          'icon' => "",//$this->getIcons($this->nav),
          'icons' => "",//$this->getIcons($this->nav, true),
          'create' => "",//$this->checkPermission($this->nav, 'create'),
          'filter' => true,
          'columns' => [
              [ "title" => "#", "width" => "5%", "filter" => ""],
              [ "title" => "Father Name", "filter" => ""],
              [ "title" => "Mother Name", "filter" => ""],
              [ "title" => "Name", "filter" => ""],
              [ "title" => "Serial Number", "width" => "5%", "filter" => ""],
              [ "title" => "Date Of Birth", "filter" => ""],
              [ "title" => "Age", "filter" => ""],
              [ "title" => "School", "filter" => ""],
              [ "title" => "Class", "filter" => ""],
              [ "title" => "Competition", "filter" => ""],
              [ "title" => "Competition Bangla", "filter" => ""],
              [ "title" => "Slot Day", "filter" => ""],
              [ "title" => "Slot Time", "filter" => ""],
              [ "title" => "Mobile", "filter" => ""],
              [ "title" => "Email", "filter" => ""],
              [ "title" => "Updated Time", "filter" => ""],
          ],
      ];
      $params['message'] = $this->getAlert();
      $params['messageType'] = $this->getAlertCSSClass();
      return view('table', $params);
    }

    public function datatable(Request $request) {
        $ajax_table = new EntryDatatable;
        return $ajax_table->table($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function show(Entry $entry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entry  $entry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry)
    {
        //
    }
}
