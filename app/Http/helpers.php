<?php
use Illuminate\Support\HtmlString;

if (! function_exists('ajax_action')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_action($action = "/")
     {
         return new HtmlString('<input type="hidden" name="_action" value="'.$action.'">');
     }
}

if (! function_exists('ajax_method')) {
    /**
     * Generate a CSRF token form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
     function ajax_method($method = "post")
     {
         return new HtmlString('<input type="hidden" name="_method" value="'.$method.'">');
     }
}
