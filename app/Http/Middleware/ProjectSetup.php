<?php

namespace App\Http\Middleware;

use Closure;
use App\Project;

class ProjectSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $service_key = getenv('APP_SERVICE_KEY');
        $project = Project::whereServiceKey($service_key)->first();
        
        session([
            'project_id' => ($project) ? $project->id : false,
            'district_id' => ($project && $project->district) ? $project->district->id : false
        ]);
        return $next($request);
    }
}
