<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
  public function entries(){
    return $this->hasMany('App\Entry');
  }
}
