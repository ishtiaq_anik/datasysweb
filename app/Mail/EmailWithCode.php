<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailWithCode extends Mailable
{
    use Queueable, SerializesModels;
    public $id;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $message)
    {
        $this->id = $id;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
     //from('info@childrensday.akij.net')->
    public function build()
    {
        return $this->markdown('emails.send_code')
            ->with([
                'message' => $this->message,
            ]);   
    }
}
