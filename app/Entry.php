<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    public function participant(){
      return $this->belongsTo('App\Participant');
    }
    public function competition(){
      return $this->belongsTo('App\Competition');
    }
}
